<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class controllerAuthorized extends CI_Controller {

 function __construct()
    {
        parent::__construct();

        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');

    }


	/*==============================================================================================================*/

	 public function index(){
	   if($this->session->userdata('logged_in')) {
	     $session_data = $this->session->userdata('logged_in');

			if($session_data['username']=='admin'){

		 	$crud = new grocery_CRUD();

			$crud->set_table('konten');
			$crud->set_subject('Konten Kain');
			$crud->unset_columns('id_konten');

				//DATA PENAMAAN ALIAS TABLE
		   	$crud->display_as('nama_konten','Nama Konten')
				 ->display_as('jenis_konten','Jenis')
				 ->display_as('asal_konten','Daerah')
				 ->display_as('gambar','Image');

			$crud->set_relation('asal_konten','wilayah_provinsi','nama');

			$crud->fields('nama_konten','jenis_konten','asal_konten','gambar')
				 ->required_fields('nama_konten','asal_konten');

         $crud->set_field_upload('gambar','assets/uploads/files');

		    $output = $crud->render();
		    $this->groceryOutputKonten($output);
			}//End if filter user
			else{
				//Jika bukan user admin
	     		redirect('controllerAuthorized/userPage', 'refresh');
			}
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('controllerLogin', 'refresh');
	   }
	 }


	 public function dataUserAPI(){
	   if($this->session->userdata('logged_in')) {
	     $session_data = $this->session->userdata('logged_in');
		 	/*$data['username'] = $session_data['username'];
		     $this->load->view('viewAuthorized', $data);*/

			/* ## Filter user menggunakan username */
			if($session_data['username']=='admin'){

		 	$crud = new grocery_CRUD();
			/*$crud->set_theme('datatables');*/
			$crud->set_table('user_api');
			$crud->set_subject('Data User API');
			$crud->unset_columns('id_user');

				//DATA PENAMAAN ALIAS TABLE
		   	$crud->display_as('nama_user','Nama User')
				 ->display_as('apikey','API Key');

			$crud->set_relation('nama_user','user','nama_user');

			$crud->fields('nama_user','apikey')
				 ->required_fields('nama_user');

		    $output = $crud->render();
		    $this->groceryOutputApi($output);
			}//End if filter user
			else{
				//Jika bukan user admin
	     		redirect('controllerAuthorized/userPage', 'refresh');
			}
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('controllerLogin', 'refresh');
	   }
	 }


	 public function dataUser(){
	   if($this->session->userdata('logged_in')) {
	     $session_data = $this->session->userdata('logged_in');
		 	/*$data['username'] = $session_data['username'];
		     $this->load->view('viewAuthorized', $data);*/

			/* ## Filter user menggunakan username */
			if($session_data['username']=='admin'){

		 	$crud = new grocery_CRUD();
			/*$crud->set_theme('datatables');*/
			$crud->set_table('user');
			$crud->set_subject('Data User');
			$crud->unset_columns('id_user')
        ->unset_columns('password');

				//DATA PENAMAAN ALIAS TABLE
		   	$crud->display_as('username','Username')
				 ->display_as('nama_user','Nama User')
				 ->display_as('points','Point')
         ->display_as('id_role','Role');

      $crud->set_relation('id_role','role','role');

			$crud->fields('username','password','nama_user','id_role')
				 ->required_fields('username');

		    $output = $crud->render();
		    $this->groceryOutputUser($output);
			}//End if filter user
			else{
				//Jika bukan user admin
	     		redirect('controllerAuthorized/userPage', 'refresh');
			}
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('controllerLogin', 'refresh');
	   }
	 }




	public function logout(){
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('controllerAuthorized', 'refresh');
 	}

	function groceryOutput($output = null){
        $this->load->view('viewAuthorized.php',$output);
    }

    function groceryOutputKonten($output = null){
          $this->load->view('viewDashboard.php',$output);
      }

      function groceryOutputUser($output = null){
            $this->load->view('viewUser.php',$output);
        }

        function groceryOutputApi($output = null){
              $this->load->view('viewApi.php',$output);
          }

}

?>

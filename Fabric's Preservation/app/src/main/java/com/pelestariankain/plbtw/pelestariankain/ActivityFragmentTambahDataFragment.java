package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentTambahDataFragment extends Fragment {
    private static final String URL_CREATE="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/kontenAdd";
    private static final String URL_GETPROVINSI="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/kontenProvinsi";
    private static final String URL_GETKabupaten="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/kontenKabupaten";
    private static final String URL_GETKecamatan="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/kontenKecamatan";
    private static final String URL_GETDesa="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/kontenDesa";
    private String URLUploadPicture = "http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/uploadPicture";

    private static final int SELECT_PICTURE = 1;

    private ArrayList<HashMap<String,String>> provinsiList;
    private ArrayList<HashMap<String,String>> kabupatenList;
    private ArrayList<HashMap<String,String>> kecamatanList;
    private ArrayList<HashMap<String,String>> desaList;

    JSONArray names = null;

    private TextInputLayout layout_edtNama;
    private TextInputLayout layout_edtJenis;
    private TextInputLayout layout_edtDeskripsi;
    private TextInputLayout layout_spinnerProvinsi;
    private TextInputLayout layout_spinnerKabupaten;
    private TextInputLayout layout_spinnerKecamatan;
    private TextInputLayout layout_spinnerDesa;

    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;

    private EditText edtNama;
    private EditText edtJenis;
    private EditText edtDeskripsi;
    private Button btnTambah;
    private Button btnUpload;
    private Button btnClear;
    private Spinner spinnerProvinsi;
    private Spinner spinnerKabupaten;
    private Spinner spinnerKecamatan;
    private Spinner spinnerDesa;
    private ImageButton btnClose;
    private ImageView imgFoto;

    private String id_user;

    public Bitmap gambar;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    public ActivityFragmentTambahDataFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_activity_fragment_tambah_data, container, false);
        sharedpreferences=getActivity().getSharedPreferences(name,mode);

        layout_edtNama=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_nama);
        layout_edtJenis=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_jenis);
        layout_edtDeskripsi=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_deskripsi);
        layout_spinnerProvinsi=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_wilayahprovinsi);
        layout_spinnerKabupaten=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_wilayahkabupaten);
        layout_spinnerKecamatan=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_wilayahkecamatan);
        layout_spinnerDesa=(TextInputLayout)v.findViewById(R.id.input_layout_tambah_wilayahdesa);


        edtNama= (EditText)v.findViewById(R.id.tambah_kain_edtNama);
        edtJenis= (EditText)v.findViewById(R.id.tambah_kain_edtJenis);
        edtDeskripsi= (EditText)v.findViewById(R.id.tambah_kain_edtDeskripsi);

        btnTambah= (Button)v.findViewById(R.id.tambah_kain_btnTambah);
        btnUpload= (Button)v.findViewById(R.id.tambah_kain_btnUploadFoto);
        btnClear= (Button)v.findViewById(R.id.tambah_kain_btnClear);
        imgFoto= (ImageView)v.findViewById(R.id.tambah_kain_imgView);
        btnClose= (ImageButton)v.findViewById(R.id.tambah_kain_imgBtnClose);

        spinnerProvinsi=(Spinner)v.findViewById(R.id.tambah_kain_spinner_wilayahprovinsi);
        spinnerKabupaten=(Spinner)v.findViewById(R.id.tambah_kain_spinner_wilayahkabupaten);
        spinnerKecamatan=(Spinner)v.findViewById(R.id.tambah_kain_spinner_wilayahkecamatan);
        spinnerDesa=(Spinner)v.findViewById(R.id.tambah_kain_spinner_wilayahdesa);


        provinsiList= new ArrayList<HashMap<String,String>>();
        kabupatenList= new ArrayList<HashMap<String,String>>();
        kecamatanList= new ArrayList<HashMap<String,String>>();
        desaList= new ArrayList<HashMap<String,String>>();

        id_user=sharedpreferences.getString("id_user","");


        new GetProvinsi().execute();
        spinnerProvinsi.setEnabled(true);
        spinnerKabupaten.setEnabled(false);
        spinnerKecamatan.setEnabled(false);
        spinnerDesa.setEnabled(false);
        edtNama.addTextChangedListener(new MyTextWatcher(edtNama));
        edtJenis.addTextChangedListener(new MyTextWatcher(edtJenis));
        edtDeskripsi.addTextChangedListener(new MyTextWatcher(edtDeskripsi));


        spinnerProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinnerProvinsi.getSelectedItem().toString().equalsIgnoreCase("Choose Province")) {
                    int idprovinsiposition = spinnerProvinsi.getSelectedItemPosition();
                    String idprovinsi = provinsiList.get(idprovinsiposition - 1).get("id");
                    Log.d("provinsi_id", idprovinsi);
                    new GetKabupaten(idprovinsi).execute();
                    spinnerProvinsi.setEnabled(false);
                    spinnerKabupaten.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerKabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinnerKabupaten.getSelectedItem().toString().equalsIgnoreCase("Choose District")) {
                    int idposition = spinnerKabupaten.getSelectedItemPosition();
                    String ID = kabupatenList.get(idposition - 1).get("id");
                    Log.d("provinsi_id", ID);
                    new GetKecamatan(ID).execute();
                    spinnerKabupaten.setEnabled(false);
                    spinnerKecamatan.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinnerKecamatan.getSelectedItem().toString().equalsIgnoreCase("Choose Sub-District")) {
                    int idposition = spinnerKecamatan.getSelectedItemPosition();
                    String ID = kecamatanList.get(idposition - 1).get("id");
                    Log.d("provinsi_id", ID);
                    new GetDesa(ID).execute();
                    spinnerKecamatan.setEnabled(false);
                    spinnerDesa.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                imgFoto.setBackgroundResource(R.drawable.noimagefound);
                imgFoto.setImageBitmap(null);
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateKecamatan() || !validateKabupaten() || !validateProvinsi()|| !validateProvinsi()|| !validateDesa()||
                    !validateNama()|| !validateDeskripsi()|| !validateJenis())
                    Toast.makeText(getActivity(),"Please select the location..",Toast.LENGTH_LONG).show();
                else{
                    if(imgFoto.getDrawable()==null || ((BitmapDrawable)imgFoto.getDrawable()).getBitmap()==null)
                        Toast.makeText(getActivity(),"Please select a picture first..",Toast.LENGTH_LONG).show();
                    else {
                        gambar=((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
                        new UploadImage().execute();
                    }
                }


            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAll();
            }
        });
        return v;
    }
    class createKainJSON extends AsyncTask<String, String, String> {
        String sukses = "";
//        String status,id_pegawai;
        String nama,jenis,deskripsi,desa,kecamatan,kabupaten,provinsi,gambar,points;
        int id_user;
//        Log.d("test : ", json.toString());
        public createKainJSON(int id_user,String nama, String jenis, String deskripsi,String desa, String kecamatan, String kabupaten, String provinsi,String gambar)
        {
            this.id_user=id_user;
            this.nama=nama;
            this.jenis=jenis;
            this.deskripsi=deskripsi;
            this.desa=desa;
            this.kecamatan=kecamatan;
            this.kabupaten=kabupaten;
            this.provinsi=provinsi;
            this.gambar=gambar;
            pDialog = new ProgressDialog(getActivity());

        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Please Wait ... Creating ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_user",Integer.toString(id_user)));
            params.add(new BasicNameValuePair("nama_konten",nama));
            params.add(new BasicNameValuePair("jenis",jenis));
            params.add(new BasicNameValuePair("deskripsi",deskripsi));
            params.add(new BasicNameValuePair("desa",desa));
            params.add(new BasicNameValuePair("kecamatan",kecamatan));
            params.add(new BasicNameValuePair("kabupaten",kabupaten));
            params.add(new BasicNameValuePair("provinsi",provinsi));
            params.add(new BasicNameValuePair("gambar", gambar));
            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "POST", params);
                if (json != null) {
                    sukses = json.getString("status");
                    jsonarray = json.getJSONArray("userData");

                    if (sukses.equalsIgnoreCase("1")) {
                        if (json != null) {
                            JSONObject jsonobject = jsonarray.getJSONObject(0);

                            points = jsonobject.getString("points");

                        }

                    }
                }}catch(JSONException e){
                    e.printStackTrace();
                }

            return null;
        }


        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(),"Data fabric added!",Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("points",points);
                editor.apply();
                clearAll();
            }
            else
            {
                Toast.makeText(getActivity(),"Failed to add!",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //=-=-=-=-=-=-=-=-=AWAL KELAS GET DEPARTMENT-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    class GetProvinsi extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Reading data provinces.. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
        public GetProvinsi()
        {
            pDialog = new ProgressDialog(getActivity());
        }
        @Override
        protected Void doInBackground(Void... arg0) {
            String sukses = "0";
            List<NameValuePair> params =new ArrayList<NameValuePair>();
//            params.add(new BasicNameValuePair("DEPARTMENT_NAME", ""));
            try {
                JSONObject json = jParser.makeHttpRequest(URL_GETPROVINSI, "GET",params);
                if (json != null) {
                    sukses = json.getString("status");
                    if (sukses.equalsIgnoreCase("1")) {
                        Log.d("Semua Nama: ", json.toString());
                        names = json.getJSONArray("kontenData");
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
                            String id = c.getString("id");
                            String nama = c.getString("nama");
                            Log.d("test : ", nama);
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("id", id);
                            map.put("nama", nama);
                            provinsiList.add(map);

                        }
                    }
                } else{
                    Toast.makeText(getActivity(), "aadsdasda", Toast.LENGTH_LONG).show();}
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            if (pDialog.isShowing())
            pDialog.dismiss();
            populateSpinner("Province",provinsiList,spinnerProvinsi );
        }
    }

    private void populateSpinner(String wilayah,ArrayList<HashMap<String,String>> List, Spinner spinner ) {
        List<String> lables = new ArrayList<String>();
        lables.add("Choose "+wilayah);
        for (int i = 0; i < List.size(); i++) {
            String str = List.get(i).get("nama");
            lables.add(str);
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, lables);
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(spinnerAdapter);
    }
    //=-=-=-=-=-=-=-=-=AWAL KELAS GET DEPARTMENT-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    class GetKabupaten extends AsyncTask<Void, Void, Void> {

        String provinsi_id;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Reading data districts.. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
        public GetKabupaten(String provinsi_id)
        {
            this.provinsi_id=provinsi_id;
            pDialog = new ProgressDialog(getActivity());

        }
        @Override
        protected Void doInBackground(Void... arg0) {
            String sukses = "0";
            List<NameValuePair> params =new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("provinsi_id", provinsi_id));
            try {
                JSONObject json = jParser.makeHttpRequest(URL_GETKabupaten, "GET",params);
                if (json != null) {
                    sukses = json.getString("status");
                    if (sukses.equalsIgnoreCase("1")) {
                        Log.d("Semua Nama: ", json.toString());
                        names = json.getJSONArray("kontenData");
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
//                            String provinsi_id = c.getString("provinsi_id");
                            String id = c.getString("id");
                            String nama = c.getString("nama");
                            Log.d("test : ", nama);
                            HashMap<String, String> map = new HashMap<String, String>();
//                            map.put("provinsi_id", "provinsi_id");
                            map.put("id", id);
                            map.put("nama", nama);
                            kabupatenList.add(map);
                        }
                    }
                } else{
                    Toast.makeText(getActivity(), "aadsdasda", Toast.LENGTH_LONG).show();}
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
//            super.onPostExecute(result);
//            if (pDialog.isShowing())
            populateSpinner("District", kabupatenList, spinnerKabupaten);
        }
    }
    class GetKecamatan extends AsyncTask<Void, Void, Void> {

        String kabupaten_id;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Reading data sub-districts.. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
        public GetKecamatan(String kabupaten_id)
        {
            this.kabupaten_id=kabupaten_id;
            pDialog = new ProgressDialog(getActivity());

        }
        @Override
        protected Void doInBackground(Void... arg0) {
            String sukses = "0";
            List<NameValuePair> params =new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("kabupaten_id", kabupaten_id));
            try {
                JSONObject json = jParser.makeHttpRequest(URL_GETKecamatan, "GET",params);
                if (json != null) {
                    sukses = json.getString("status");
                    if (sukses.equalsIgnoreCase("1")) {
                        Log.d("Semua Nama: ", json.toString());
                        names = json.getJSONArray("kontenData");
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
//                            String provinsi_id = c.getString("provinsi_id");
                            String id = c.getString("id");
                            String nama = c.getString("nama");
                            Log.d("test : ", nama);
                            HashMap<String, String> map = new HashMap<String, String>();
//                            map.put("provinsi_id", "provinsi_id");
                            map.put("id", id);
                            map.put("nama", nama);
                            kecamatanList.add(map);
                        }
                    }
                } else{
                    Toast.makeText(getActivity(), "aadsdasda", Toast.LENGTH_LONG).show();}
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
//            super.onPostExecute(result);
//            if (pDialog.isShowing())
            populateSpinner("Sub-District", kecamatanList, spinnerKecamatan);
        }

    }
    class GetDesa extends AsyncTask<Void, Void, Void> {

        String kecamatan_id;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Reading data villages.. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
        public GetDesa(String kecamatan_id)
        {
            this.kecamatan_id=kecamatan_id;
            pDialog = new ProgressDialog(getActivity());

        }
        @Override
        protected Void doInBackground(Void... arg0) {
            String sukses = "0";
            List<NameValuePair> params =new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("kecamatan_id", kecamatan_id));
            try {
                JSONObject json = jParser.makeHttpRequest(URL_GETDesa, "GET", params);
                if (json != null) {
                    sukses = json.getString("status");
                    if (sukses.equalsIgnoreCase("1")) {
                        Log.d("Semua Nama: ", json.toString());
                        names = json.getJSONArray("kontenData");
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
//                            String provinsi_id = c.getString("provinsi_id");
                            String id = c.getString("id");
                            String nama = c.getString("nama");
                            Log.d("test : ", nama);
                            HashMap<String, String> map = new HashMap<String, String>();
//                            map.put("provinsi_id", "provinsi_id");
                            map.put("id", id);
                            map.put("nama", nama);
                            desaList.add(map);
                        }
                    }
                } else{
                    Toast.makeText(getActivity(), "aadsdasda", Toast.LENGTH_LONG).show();}
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
//            super.onPostExecute(result);
//            if (pDialog.isShowing())

            populateSpinner("Village", desaList, spinnerDesa);
        }

    }
    void clearAll(){
        edtNama.setText("");
        edtJenis.setText("");
        edtDeskripsi.setText("");


        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, new ArrayList<String>());

        spinnerDesa.setAdapter(spinnerAdapter);
        spinnerKecamatan.setAdapter(spinnerAdapter);
        spinnerKabupaten.setAdapter(spinnerAdapter);
        spinnerProvinsi.setSelection(0);
        spinnerDesa.setEnabled(false);
        spinnerProvinsi.setEnabled(true);
        desaList.removeAll(desaList);
        kecamatanList.removeAll(kecamatanList);
        kabupatenList.removeAll(kabupatenList);
    }
    //#################################KELAS UPLOAD GAMBAR############################################
    class UploadImage extends AsyncTask<Void, Void, String> {
        int sukses = 0;
        String image;
        public UploadImage() {
            this.image=System.currentTimeMillis() + ".jpg";
            pDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Please Wait ..., Uploading Images ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            gambar.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("base64", encodedImage));
            nameValuePairs.add(new BasicNameValuePair("ImageName", image));
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(URLUploadPicture);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                String st = EntityUtils.toString(response.getEntity());
                Log.d("Entity: ",st);
            } catch (Exception e) {
                Log.v("log_tag", "Error in http connection " + e.toString());
            }
            return "Success";
        }
        @Override
        protected void onPostExecute(String aVoid) {
//            super.onPostExecute(aVoid);
            pDialog.dismiss();
            new createKainJSON(Integer.parseInt(id_user),edtNama.getText().toString(), edtJenis.getText().toString(), edtDeskripsi.getText().toString(),
                    spinnerDesa.getSelectedItem().toString(), spinnerKecamatan.getSelectedItem().toString(),
                    spinnerKabupaten.getSelectedItem().toString(), spinnerProvinsi.getSelectedItem().toString(),image).execute();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_OK && requestCode==SELECT_PICTURE && data!=null && data.getData()!=null) {
            Bitmap bitmap = getPath(data.getData());
            imgFoto.setImageBitmap(bitmap);

        }
    }
    private Bitmap getPath(Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =getActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String filePath = cursor.getString(column_index);
        cursor.close();
        // Convert file path into bitmap image using below line.
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        return bitmap;
    }
    private boolean validateNama() {
        if (edtNama.getText().toString().trim().isEmpty()) {
            layout_edtNama.setError("Name cannot be filled empty");
            requestFocus(edtNama);
            return false;
        } else {
            layout_edtNama.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateJenis() {
        if (edtJenis.getText().toString().trim().isEmpty()) {
            layout_edtJenis.setError("Type cannot be filled empty");
            requestFocus(edtJenis);
            return false;
        } else {
            layout_edtJenis.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateDeskripsi() {
        if (edtDeskripsi.getText().toString().trim().isEmpty()) {
            layout_edtDeskripsi.setError("Description cannot be filled empty");
            requestFocus(edtDeskripsi);
            return false;
        } else {
            layout_edtDeskripsi.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateProvinsi() {
        if(spinnerProvinsi.getSelectedItem()==null) return false;
        else{
            if (spinnerProvinsi.getSelectedItem().toString().trim().isEmpty() ||
                    spinnerProvinsi.getSelectedItem().toString()=="Choose Province") {

                layout_spinnerProvinsi.setError("Description cannot be filled empty");
                requestFocus(spinnerProvinsi);
                return false;
            } else {
                layout_spinnerProvinsi.setErrorEnabled(false);
            }

            return true;
        }
    }
    private boolean validateKabupaten() {
        if(spinnerKabupaten.getSelectedItem()==null) return false;
        else {
            if (spinnerKabupaten.getSelectedItem().toString().trim().isEmpty() ||
                    spinnerKabupaten.getSelectedItem().toString() == "Choose District") {
                layout_spinnerKabupaten.setError("Description cannot be filled empty");
                requestFocus(spinnerKabupaten);
                return false;
            } else {
                layout_spinnerKabupaten.setErrorEnabled(false);
            }
            return true;
        }
    }
    private boolean validateKecamatan() {
        if(spinnerKecamatan.getSelectedItem()==null) return false;
        else {
            if (spinnerKecamatan.getSelectedItem().toString().trim().isEmpty() ||
                    spinnerKecamatan.getSelectedItem().toString() == "Choose Sub-District") {
                layout_spinnerKecamatan.setError("Description cannot be filled empty");
                requestFocus(spinnerKecamatan);
                return false;
            } else {
                layout_spinnerKecamatan.setErrorEnabled(false);
            }
            return true;
        }
    }
    private boolean validateDesa() {
        if(spinnerDesa.getSelectedItem()==null) return false;
        else {
            if (spinnerDesa.getSelectedItem().toString().trim().isEmpty() ||
                    spinnerDesa.getSelectedItem().toString() == "Choose Village") {
                layout_spinnerDesa.setError("Description cannot be filled empty");
                requestFocus(spinnerDesa);
                return false;
            } else {
                layout_spinnerDesa.setErrorEnabled(false);
            }
            return true;
        }
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.tambah_kain_edtNama:
                    validateNama();
                    break;
                case R.id.tambah_kain_edtJenis:
                    validateJenis();
                    break;
                case R.id.tambah_kain_edtDeskripsi:
                    validateDeskripsi();
                    break;
                case R.id.tambah_kain_spinner_wilayahprovinsi:
                    validateProvinsi();
                    break;
                case R.id.tambah_kain_spinner_wilayahkabupaten:
                    validateKabupaten();
                case R.id.tambah_kain_spinner_wilayahkecamatan:
                    validateKecamatan();
                case R.id.tambah_kain_spinner_wilayahdesa:
                    validateDesa();
                    break;
            }
        }
    }

}

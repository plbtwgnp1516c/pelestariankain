package com.pelestariankain.plbtw.pelestariankain;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pelestariankain.plbtw.pelestariankain.NavDrawer.HomeFragment;

/**
 * Created by Joko Adi Hartono on 5/8/2016.
 */
public class ViewPagerUserAdapter extends FragmentStatePagerAdapter {
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerUserAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            HomeFragment homeFragment = new HomeFragment();
            return homeFragment;
        }
        else if(position==1)
        {
            ActivityFragmentListLogUserFragment activityFragmentListLogUserFragment= new ActivityFragmentListLogUserFragment();
            return activityFragmentListLogUserFragment;
        }
        else
        {
            ActivityFragmentListLogRedeemFragment activityFragmentListLogRedeemFragment= new ActivityFragmentListLogRedeemFragment();
            return activityFragmentListLogRedeemFragment;
        }




    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}

package com.pelestariankain.plbtw.pelestariankain.user;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pelestariankain.plbtw.pelestariankain.ActivityTab;
import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ForgotPasswordActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private String URLForgotPassowrdRead = "http://xiaokwee.5gbfree.com/MartabakMobile/user_controller/read.php";
    private TextInputLayout layout_username;
    private TextInputLayout layout_email;
    private TextInputLayout layout_creditcard;
    private TextInputLayout layout_phonenumber;

    private EditText edtUsername;
    private EditText edtEmail;
    private EditText edtCreditCard;
    private EditText edtPhonenumber;

    private Button btnGetPass;
    private TextView txtPassword;

    private static final String TAG_PESAN = "message";
    private static final String TAG_HASIL = "result";

    ProgressDialog pDialog;

    JSONParser jParser = new JSONParser();
    JSONArray names = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mToolbar=(Toolbar)findViewById(R.id.toolbar);
        //setSupportActionBar(mToolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setObjek();
        btnGetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CheckData(edtUsername.getText().toString(),edtCreditCard.getText().toString(),
                        edtEmail.getText().toString(),edtPhonenumber.getText().toString()).execute();
            }
        });

    }
    public void setObjek(){
        layout_creditcard=(TextInputLayout)findViewById(R.id.forgot_layout_creditcard);
        layout_email=(TextInputLayout)findViewById(R.id.forgot_layout_email);
        layout_phonenumber=(TextInputLayout)findViewById(R.id.forgot_layout_phonenumber);
        layout_username=(TextInputLayout)findViewById(R.id.forgot_layout_username);

        edtUsername=(EditText)findViewById(R.id.forgot_edittext_username);
        edtCreditCard=(EditText)findViewById(R.id.forgot_edittext_creditcard);
        edtEmail=(EditText)findViewById(R.id.forgot_edittext_email);
        edtPhonenumber=(EditText)findViewById(R.id.forgot_edittext_phonenumber);

        btnGetPass=(Button)findViewById(R.id.forgot_button_getpassword);
        txtPassword=(TextView)findViewById(R.id.forgot_textview_password);

        edtUsername.addTextChangedListener(new MyTextWatcher(edtUsername));
        edtPhonenumber.addTextChangedListener(new MyTextWatcher(edtPhonenumber));
        edtEmail.addTextChangedListener(new MyTextWatcher(edtEmail));
        edtCreditCard.addTextChangedListener(new MyTextWatcher(edtCreditCard));
    }

    class CheckData extends AsyncTask<String,String,String> {
        public int sukses = 0;
        String USERNAME,EMAIL,CREDITCARD,PHONENUMBER;
        String password;

        public CheckData(String USERNAME,String CREDITCARD, String EMAIL, String PHONENUMBER){
            this.USERNAME=USERNAME;
            this.CREDITCARD=CREDITCARD;
            this.EMAIL=EMAIL;
            this.PHONENUMBER=PHONENUMBER;
            pDialog = new ProgressDialog(ForgotPasswordActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Checking. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("USERNAME", USERNAME));
            params.add(new BasicNameValuePair("CREDITCARD", CREDITCARD));
            params.add(new BasicNameValuePair("EMAIL", EMAIL));
            params.add(new BasicNameValuePair("PHONENUMBER", PHONENUMBER));
            try{
                JSONObject json = jParser.makeHttpRequest(URLForgotPassowrdRead,"GET",params);
                if(json!=null){
                    sukses=json.getInt(TAG_PESAN);

                    if(sukses==1){
                        Log.d("Semua Nama: ", json.toString());
                        names= json.getJSONArray(TAG_HASIL);
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
                            password= c.getString("PASSWORD");
                        }
                    }
                }
            }catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if(sukses==1){
                    txtPassword.setText(password);
            }
            else{
                Toast.makeText(getApplicationContext(), "Please check again your data!", Toast.LENGTH_LONG).show();
                txtPassword.setText("");

            }

        }
    }
    private boolean validateUsername() {
        if (edtUsername.getText().toString().trim().isEmpty()) {
            layout_username.setError("Username Cannot be Blank");
            requestFocus(edtUsername);
            return false;
        } else {
            layout_username.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateCreditCard() {
        if (edtCreditCard.getText().toString().trim().isEmpty()) {
            layout_creditcard.setError("Credit Card Cannot be Blank");
            requestFocus(edtCreditCard);
            return false;
        } else {
            layout_creditcard.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateEmail() {
        if (edtEmail.getText().toString().trim().isEmpty()) {
            layout_email.setError("Email Cannot be Blank");
            requestFocus(edtEmail);
            return false;
        } else {
            layout_email.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validatePhonenumber() {
        if (edtPhonenumber.getText().toString().trim().isEmpty()) {
            layout_phonenumber.setError("Phone Number Cannot be Blank");
            requestFocus(edtPhonenumber);
            return false;
        } else {
            layout_phonenumber.setErrorEnabled(false);
        }

        return true;
    }
    private void GetPassword() {
        if(!validateUsername()) {
            return;
        }

        if(!validatePhonenumber()){
            return;
        }

        if(!validateEmail()){
            return;
        }

        if(!validateCreditCard()){
            return;
        }

    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    public class MyTextWatcher implements TextWatcher{
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forgot_password, menu);
        return true;
    }
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}

package com.pelestariankain.plbtw.pelestariankain.user;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GoogleApiAvailability;
//import com.martabakmobile.martabakmobile.GMC.QuickstartPreferences;
//import com.martabakmobile.martabakmobile.GMC.RegistrationIntentService;
//import com.martabakmobile.martabakmobile.JSONParser;
//import com.martabakmobile.martabakmobile.MainActivity;
//import com.martabakmobile.martabakmobile.R;

import com.pelestariankain.plbtw.pelestariankain.ActivityTab;
import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.NavDrawer.MainMenu;
import com.pelestariankain.plbtw.pelestariankain.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//import android.support.v7.widget.Toolbar;

public class LoginActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";
    private BroadcastReceiver mRegistrationBroadcastReciver;
    private LinearLayout mRegistrationProgressBar;

    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;

    private TextInputLayout layoutUsername;
    private TextInputLayout layoutPassword;
    private EditText edtUsername;
    private EditText edtPassword;
    private CheckBox checkPassword;
    private Button btnLogin;
    private TextView txtForgotPassword;
    private TextView txtRegister;


    private String UrlLoginInRead = "http://pelestariankain.esy.es/plbtw-rest/index.php/private/User/LoginUser";
    private static final String TAG_USERNAME = "USERNAME";
    private static final String TAG_PASSWORD = "PASSWORD";

    private static final String TAG_PESAN = "status";
    private static final String TAG_HASIL = "userData";
    private  ProgressDialog pDialog;

    JSONParser jParser = new JSONParser();
    JSONArray names = null;
    class SignInCheck extends AsyncTask<String,String,String> {
        public int sukses = 0;
        String USERNAME,PASSWORD;
        String id_user,nama,username,password,email,alamat,no_telp,points,gambar,sisaRedeem,currentRedeem;

        public SignInCheck(String USERNAME,String PASSWORD){
            this.USERNAME=USERNAME;
            this.PASSWORD=PASSWORD;
            pDialog = new ProgressDialog(LoginActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Logging In. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", USERNAME));
            params.add(new BasicNameValuePair("password", PASSWORD));
            try{
                JSONObject json = jParser.makeHttpRequest(UrlLoginInRead,"POST",params);
                if(json!=null){
                    sukses=json.getInt(TAG_PESAN);

                    if(sukses==1){
                        Log.d("","---->JSON:"+json.toString());
                        names= json.getJSONArray(TAG_HASIL);
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
                            id_user= c.getString("id_user");
                            username= c.getString("username");
                            password= c.getString("password");
                            nama = c.getString("nama");
                            email = c.getString("email");
                            alamat = c.getString("alamat");
                            points = c.getString("points");
                            no_telp = c.getString("no_telp");
                            gambar = c.getString("gambar");
                            sisaRedeem = c.getString("sisaRedeem");
                            currentRedeem = c.getString("currentRedeem");


                        }
                    }
                }
            }catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if(sukses==1){
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("id_user",id_user);
                editor.putString("username",username);
                editor.putString("password",password);
                editor.putString("nama",nama);
                editor.putString("email",email);
                editor.putString("alamat",alamat);
                editor.putString("points",points);
                editor.putString("no_telp",no_telp);
                editor.putString("gambar", gambar);
                editor.putString("sisaRedeem", sisaRedeem);
                editor.putString("currentRedeem", currentRedeem);
                editor.apply();
                finish();
                Toast.makeText(getApplicationContext(), "Welcome Back " + nama, Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(),MainMenu.class);
                startActivity(i);
            }
            else {
                Toast.makeText(getApplicationContext(), "Username or Password is Wrong", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginmenu);
        mToolbar=(Toolbar)findViewById(R.id.toolbar);

        sharedpreferences=getSharedPreferences(name,mode);
        btnLogin=(Button)findViewById(R.id.login_button_login);
        layoutUsername= (TextInputLayout)findViewById(R.id.login_layout_username);
        layoutPassword= (TextInputLayout)findViewById(R.id.login_layoutpassword);
        edtUsername=(EditText)findViewById(R.id.login_edittext_username);
        edtPassword=(EditText)findViewById(R.id.login_edittext_password);
        checkPassword=(CheckBox)findViewById(R.id.login_checkbox_showpass);
        txtForgotPassword=(TextView)findViewById(R.id.login_text_forgotpass);
        txtRegister=(TextView)findViewById(R.id.login_text_register);
        edtUsername.addTextChangedListener(new MyTextWatcher(edtUsername));
        edtPassword.addTextChangedListener(new MyTextWatcher(edtPassword));


        //langsung login kalau ada sharedpreferences
        if(sharedpreferences!=null)
        {
            Log.d("Sharedpreference","!null");
//            edtUsername.setText(sharedpreferences.getString("username",""));
//            edtPassword.setText(sharedpreferences.getString("password",""));
            if(!edtUsername.getText().toString().equalsIgnoreCase("") && edtPassword.getText().toString().equalsIgnoreCase("")) {
                new SignInCheck(sharedpreferences.getString("username", ""), sharedpreferences.getString("password", "")).execute();
            }
        }

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));

            }
        });
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignIn();

            }
        });
        checkPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPassword.setSelection(edtPassword.getText().length());
                } else {
                    edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edtPassword.setSelection(edtPassword.getText().length());
                }
            }
        });

    }
    @Override
    protected void onPause()
    {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReciver);
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReciver, new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }
//    private boolean checkPlayServices()
//    {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
//        if(resultCode!= ConnectionResult.SUCCESS)
//        {
//            Log.d("connetion ","true ");
//            if(apiAvailability.isUserResolvableError(resultCode))
//            {
//                apiAvailability.getErrorDialog(this,resultCode,PLAY_SERVICES_RESOLUTION_REQUEST).show();
//
//            }
//            else
//            {
//                Log.i(TAG, "This decive is not supported.");
//                finish();
//            }
//            return false;
//        }
//        Log.d("connetion ","? ");
//        return true;
//    }
   // public void
    private boolean validateUsername() {
        if (edtUsername.getText().toString().trim().isEmpty()) {
            layoutUsername.setError("Username Cannot be Blank");
            requestFocus(edtUsername);
            return false;
        } else {
            layoutUsername.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (edtPassword.getText().toString().trim().isEmpty()) {
            layoutPassword.setError("Password Cannot be Blank");
            requestFocus(edtPassword);
            return false;
        } else{
            layoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private void SignIn() {
        if(!validateUsername()) {
            return;
        }

        if(!validatePassword()){
            return;
        }

        new SignInCheck(edtUsername.getText().toString(),edtPassword.getText().toString()).execute();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.login_edittext_username:
                    validateUsername();
                    break;
                case R.id.login_edittext_password:
                    validatePassword();
                    break;
            }
        }
    }

}

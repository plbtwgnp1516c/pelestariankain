package com.pelestariankain.plbtw.pelestariankain.NavDrawer;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pelestariankain.plbtw.pelestariankain.R;
import com.pelestariankain.plbtw.pelestariankain.user.LoginActivity;
import com.squareup.picasso.Picasso;


public class HomeFragment extends Fragment {

    private Button btnlogout;
    private TextView detaildialogTxtUsername;
    private TextView detaildialogTxtNama;
    private TextView detaildialogTxtEmail;
    private TextView detaildialogTxtAlamat;
    private TextView detaildialogTxtNoTelp;
    private TextView detaildialogTxtPoints;
    private TextView detaildialogTxtRedeem;
    private ImageView detaildialogImgGambar;
    private ImageView imgProfile;
    private Button detaildialogBtnClose;

    private TextView txtProfile;
    private TextView txtUserName;

    private String username;
    private String nama;
    private String email;
    private String alamat;
    private String noTelp;
    private String points;
    private String gambar;
    private String sisaRedeem;
    private String currentRedeem;
    private String url;

    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;
    private String spnama;
    private String spusername;
    private String spflag;
    private Dialog dialog;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_home, container, false);

        sharedpreferences=getActivity().getSharedPreferences(name,mode);
        txtProfile= (TextView)v.findViewById(R.id.fragment_home_textProfile);
        txtUserName= (TextView)v.findViewById(R.id.fragment_home_Username);
        imgProfile= (ImageView)v.findViewById(R.id.fragment_home_imgprof);


        username=sharedpreferences.getString("username","");
        nama=sharedpreferences.getString("password","");
        email=sharedpreferences.getString("email","");
        alamat=sharedpreferences.getString("alamat","");
        noTelp=sharedpreferences.getString("no_telp","");
        points=sharedpreferences.getString("points","");
        gambar=sharedpreferences.getString("gambar","");
        sisaRedeem=sharedpreferences.getString("sisaRedeem","");
        currentRedeem=sharedpreferences.getString("currentRedeem","");

        url = "http://pelestariankain.esy.es/plbtw-rest/fotoprofile/"+gambar;
        Picasso.with(getActivity()).load(url).into(imgProfile);

        txtUserName.setText(username);
        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.detaildialogprofile);
                dialog.setTitle("Detail User'Profile");

                detaildialogTxtUsername=(TextView)dialog.findViewById(R.id.detildialogprofile_txtUsername);
                detaildialogTxtNama=(TextView)dialog.findViewById(R.id.detildialogprofile_txtNama);
                detaildialogTxtEmail=(TextView)dialog.findViewById(R.id.detildialogprofile_txtEmail);
                detaildialogTxtAlamat=(TextView)dialog.findViewById(R.id.detildialogprofile_txtAlamat);
                detaildialogTxtPoints=(TextView)dialog.findViewById(R.id.detildialogprofile_txtPoints);
                detaildialogTxtNoTelp=(TextView)dialog.findViewById(R.id.detildialogprofile_txtNoTelp);
                detaildialogBtnClose=(Button)dialog.findViewById(R.id.detildialogprofile_btnClose);
                detaildialogTxtRedeem=(TextView)dialog.findViewById(R.id.detildialogprofile_txtRedeem);
                detaildialogImgGambar=(ImageView)dialog.findViewById(R.id.detildialogprofile_data_imgView);

                Picasso.with(getActivity()).load(url).into(detaildialogImgGambar);


                detaildialogTxtAlamat.setText("Address : " + alamat);
                detaildialogTxtEmail.setText("Email : "+email);
                detaildialogTxtNama.setText("Name : "+nama);
                detaildialogTxtNoTelp.setText("Phone Number : "+noTelp);
                detaildialogTxtPoints.setText("Points : "+points);
                detaildialogTxtRedeem.setText("You have  : "+sisaRedeem+" times remaining to redeem items");
                detaildialogTxtUsername.setText(username);

                detaildialogBtnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        });
        btnlogout=(Button)v.findViewById(R.id.fragment_home_btnlogout);

        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Logout");
                builder.setMessage("Are you sure to logout ?");
                builder.setCancelable(false);

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        FragmentDrawer.adapter.selected_item = 0;

                        sharedpreferences = getActivity().getSharedPreferences(name, mode);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("username", "");
                        editor.putString("password", "");
                        editor.putString("nama", "");
                        editor.putString("email", "");
                        editor.putString("alamat", "");
                        editor.putString("points", "0");
                        editor.putString("gambar", "");
                        editor.putString("no_telp", "");
                        editor.putString("sisaRedeem", "");
                        editor.putString("currentRedeem", "");


                        editor.commit();

                        spnama = sharedpreferences.getString("ceknama", "");
                        spusername = sharedpreferences.getString("cekusername", "");
                        Toast.makeText(getActivity(), "Logout Success ", Toast.LENGTH_SHORT).show();

                        getActivity().finish();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                        FragmentDrawer.adapter.selected_item = FragmentDrawer.positionbefore;
                        FragmentDrawer.recyclerView.getAdapter().notifyDataSetChanged();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}

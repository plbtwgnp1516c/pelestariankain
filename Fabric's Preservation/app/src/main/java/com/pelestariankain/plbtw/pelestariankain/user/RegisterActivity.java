package com.pelestariankain.plbtw.pelestariankain.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pelestariankain.plbtw.pelestariankain.ActivityTab;
import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {
    private String URLRegisterCreate = "http://pelestariankain.esy.es/plbtw-rest/index.php/private/User/SignUp";
    private String URLRegisterRead = "http://pelestariankain.esy.es/plbtw-rest/index.php/private/User/GetDataUser";
    private String URLUploadPicture = "http://pelestariankain.esy.es/plbtw-rest/index.php/private/User/uploadPicture";

    private TextInputLayout layout_username;
    private TextInputLayout layout_password;
    private TextInputLayout layout_repassword;
    private TextInputLayout layout_fullname;
    private TextInputLayout layout_email;
    private TextInputLayout layout_alamat;
    private TextInputLayout layout_phonenumber;

    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtRePass;
    private EditText edtEmail;
    private EditText edtAlamat;
    private EditText edtFullname;
    private EditText edtPhonenumber;

    private Button btnRegister;
    private TextView txtInsert;

    private static final String TAG_PESAN = "status";
    private static final String TAG_HASIL = "userData";
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray names = null;
    private ImageView imgFoto;
    private Button btnUpload;
    public Bitmap gambar;

    private Toolbar mToolbar;
    private String userCek;
    private static final int SELECT_PICTURE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mToolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgFoto= (ImageView)findViewById(R.id.register_user_imgView);
        btnUpload= (Button)findViewById(R.id.register_user_btnUploadFoto);

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);
            }
        });

        setObject();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUp();

            }
        });


    }
    private Bitmap getPath(Uri uri) {

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String filePath = cursor.getString(column_index);
        cursor.close();
        // Convert file path into bitmap image using below line.
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        return bitmap;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode==SELECT_PICTURE && data!=null && data.getData()!=null) {
            imgFoto.setBackgroundResource(0);
            Bitmap bitmap = getPath(data.getData());
            imgFoto.setImageBitmap(bitmap);

        }
    }
    class RegisterCheck extends AsyncTask<String,String,String> {
        public String sukses ="0";
        String USERNAME;

        public RegisterCheck(String USERNAME){
            this.USERNAME=USERNAME;
            pDialog = new ProgressDialog(RegisterActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Checking. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", USERNAME));
            try{
                JSONObject json = jParser.makeHttpRequest(URLRegisterRead,"POST",params);
                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("1")){
                        Log.d("Semua Nama: ", json.toString());
                        names= json.getJSONArray(TAG_HASIL);
                        for(int i =0; i < names.length();i++){
                            JSONObject c = names.getJSONObject(i);
                            userCek= c.getString("USERNAME");
                        }
                    }
                }
            }catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("0")){
                if(imgFoto.getDrawable()==null || ((BitmapDrawable)imgFoto.getDrawable()).getBitmap()==null)
                    Toast.makeText(getApplicationContext(),"Please select a picture first..",Toast.LENGTH_LONG).show();
                else {
                    gambar=((BitmapDrawable) imgFoto.getDrawable()).getBitmap();
                    new UploadImage().execute();
                }

            }
            else{
                Toast.makeText(getApplicationContext(),"Username has been used by another user please change your username",Toast.LENGTH_LONG).show();

            }

        }
    }
    //#################################KELAS UPLOAD GAMBAR############################################
    class UploadImage extends AsyncTask<Void, Void, String> {
        int sukses = 0;
        String image;
        public UploadImage() {
            this.image=System.currentTimeMillis() + ".jpg";
            pDialog = new ProgressDialog(RegisterActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Please Wait ..., Uploading Images ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            gambar.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("base64", encodedImage));
            nameValuePairs.add(new BasicNameValuePair("ImageName", image));
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(URLUploadPicture);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                String st = EntityUtils.toString(response.getEntity());
                Log.d("Entity: ",st);
            } catch (Exception e) {
                Log.v("log_tag", "Error in http connection " + e.toString());
            }
            return "Success";
        }
        @Override
        protected void onPostExecute(String aVoid) {
//            super.onPostExecute(aVoid);
            pDialog.dismiss();
            new Register(edtUsername.getText().toString(),edtPassword.getText().toString(),
                    edtFullname.getText().toString(),edtEmail.getText().toString(),edtAlamat.getText().toString()
                    ,edtPhonenumber.getText().toString(),image)
                    .execute();

        }
    }
    //=-=-=-=-=-=-=-=-=KELAS SIGN UP USER-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    class Register extends AsyncTask<String,String,String> {
        int sukses = 0;
        String user, password, fullname, email,alamat,phonenumber,image;
        public Register(String user, String password, String fullname, String email
                ,String alamat,String phonenumber,String image) {
            this.user = user;
            this.password = password;
            this.fullname = fullname;
            this.email = email;
            this.alamat = alamat;
            this.phonenumber = phonenumber;
            this.image = image;
            pDialog = new ProgressDialog(RegisterActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Signing Up. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", user));
            params.add(new BasicNameValuePair("password", password));
            params.add(new BasicNameValuePair("nama", fullname));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("alamat", alamat));
            params.add(new BasicNameValuePair("no_telp", phonenumber));
            params.add(new BasicNameValuePair("gambar", image));

            try {
                JSONObject json = jParser.makeHttpRequest(URLRegisterCreate, "POST", params);
                if (json != null) {
                    sukses = json.getInt(TAG_PESAN);
                    if (sukses == 1) {
                        Log.d("Semua Nama: ", json.toString());
//                        names = json.getJSONArray(TAG_HASIL);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
        }
    }
    private void setObject(){
        layout_alamat= (TextInputLayout)findViewById(R.id.register_layout_alamat);
        layout_email= (TextInputLayout)findViewById(R.id.register_layout_email);
        layout_username= (TextInputLayout)findViewById(R.id.register_layout_username);
        layout_phonenumber= (TextInputLayout)findViewById(R.id.register_layout_phonenumber);
        layout_fullname= (TextInputLayout)findViewById(R.id.register_layout_fullname);
        layout_password= (TextInputLayout)findViewById(R.id.register_layout_password);
        layout_repassword= (TextInputLayout)findViewById(R.id.register_layout_repassword);

        edtAlamat= (EditText)findViewById(R.id.register_edittext_alamat);
        edtUsername= (EditText)findViewById(R.id.register_edittext_username);
        edtPassword= (EditText)findViewById(R.id.register_edittext_password);
        edtRePass= (EditText)findViewById(R.id.register_edittext_repassword);
        edtFullname= (EditText)findViewById(R.id.register_edittext_fullname);
        edtPhonenumber= (EditText)findViewById(R.id.register_edittext_phonenumber);
        edtEmail= (EditText)findViewById(R.id.register_edittext_email);

        btnRegister=(Button)findViewById(R.id.register_button_register);
        txtInsert=(TextView)findViewById(R.id.register_textview_viewinsert);

        edtUsername.addTextChangedListener(new MyTextWatcher(edtUsername));
        edtPassword.addTextChangedListener(new MyTextWatcher(edtPassword));
        edtRePass.addTextChangedListener(new MyTextWatcher(edtRePass));
        edtFullname.addTextChangedListener(new MyTextWatcher(edtFullname));
        edtPhonenumber.addTextChangedListener(new MyTextWatcher(edtPhonenumber));
        edtEmail.addTextChangedListener(new MyTextWatcher(edtEmail));
        edtAlamat.addTextChangedListener(new MyTextWatcher(edtAlamat));

    }
    private boolean validateUsername() {
        if (edtUsername.getText().toString().trim().isEmpty()) {
            layout_username.setError("Username Cannot be Blank");
            requestFocus(edtUsername);
            return false;
        } else {
            layout_username.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (edtPassword.getText().toString().trim().isEmpty()) {
            layout_password.setError("Password Cannot be Blank");
            requestFocus(edtPassword);
            return false;
        } else if(edtPassword.getText().length()<4) {
            layout_password.setError("Password Cannot must be in 4 - 12 Characters");
            requestFocus(edtPassword);
            return false;
        } else{
            layout_password.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateKonfirmPass(){
        if(edtRePass.getText().toString().trim().isEmpty()){
            layout_repassword.setError("Re-type your Password");
            requestFocus(edtRePass);
            return false;
        } else if(edtRePass.getText().length()<4){
            layout_repassword.setError("Re-type your password in 4 - 12 Characters");
            requestFocus(edtRePass);
            return false;
        } else if(!edtPassword.getText().toString().equals(edtRePass.getText().toString())){
            layout_repassword.setError("Password Mismatch");
            requestFocus(edtRePass);
            return false;
        } else {
            layout_repassword.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateNamalengkap() {
        if (edtFullname.getText().toString().trim().isEmpty()) {
            layout_fullname.setError("Fullname Cannot be Blank");
            requestFocus(edtFullname);
            return false;
        } else {
            layout_fullname.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateCreditCard() {
        if (edtAlamat.getText().toString().trim().isEmpty()) {
            layout_alamat.setError("Credit Card Cannot be Blank");
            requestFocus(edtAlamat);
            return false;
        } else {
            layout_alamat.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateEmail() {
        if (edtEmail.getText().toString().trim().isEmpty()) {
            layout_email.setError("Email Cannot be Blank");
            requestFocus(edtEmail);
            return false;
        } else {
            layout_email.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validatePhonenumber() {
        if (edtPhonenumber.getText().toString().trim().isEmpty()) {
            layout_phonenumber.setError("Phone Number Cannot be Blank");
            requestFocus(edtPhonenumber);
            return false;
        } else {
            layout_phonenumber.setErrorEnabled(false);
        }

        return true;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private void SignUp() {
        if(!validateUsername()) {
            return;
        }

        if(!validatePassword()){
            return;
        }

        if(!validateKonfirmPass()){
            return;
        }

        if(!validateNamalengkap()){
            return;
        }

        if(!validatePhonenumber()){
            return;
        }

        if(!validateEmail()){
            return;
        }

        if(!validateCreditCard()){
            return;
        }
        new RegisterCheck(edtUsername.getText().toString()).execute();
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            switch (view.getId()) {
                case R.id.register_edittext_username:
                    validateUsername();
                    break;
                case R.id.register_edittext_password:
                    validatePassword();
                    break;
                case R.id.register_edittext_repassword:
                    validateKonfirmPass();
                    break;
                case R.id.register_edittext_fullname:
                    validateNamalengkap();
                    break;
                case R.id.register_edittext_alamat:
                    validateCreditCard();
                case R.id.register_edittext_email:
                    validateEmail();
                case R.id.register_edittext_phonenumber:
                    validatePhonenumber();
                    break;
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}

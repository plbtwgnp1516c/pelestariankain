package com.pelestariankain.plbtw.pelestariankain.NavDrawer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pelestariankain.plbtw.pelestariankain.ActivityTab;
import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;
import com.pelestariankain.plbtw.pelestariankain.UserTab;
import com.pelestariankain.plbtw.pelestariankain.ViewPagerUserAdapter;
import com.pelestariankain.plbtw.pelestariankain.tab.SlidingTabLayout;
import com.pelestariankain.plbtw.pelestariankain.user.LoginActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class MainMenu extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener{

    private static String TAG = MainMenu.class.getSimpleName();

    public static TextView nama;
    private int tes=10;
    private Toolbar mToolbar;

    private FragmentDrawer drawerFragment;
    private static final String TAG_DEPARTMENT_NAME = "department_name";

    public static String dptName = "";
    public static String dptProfile= "";
    public static String namaprofile= "";
    public static String dptID = "";
    public static String username;
    public static String role;

    private int flagnosetup;
    private int flagnosummary;

    public static String sprole;

    private static final String TAG_PESAN = "message";
    private static final String TAG_HASIL = "result";
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray names = null;
    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;
    private String spnama;
    private String spusername;
    private String spflag;
    private String spdept;





    //----------------==================AKHIR KELAS GET GRUP ITEM==================---------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        sharedpreferences=getSharedPreferences(name, mode);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);


        displayView(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;

        if(position==0)
        {
            fragment = new UserTab();
            FragmentDrawer.positionbefore=0;
        }
        else if(position==1){
            fragment = new ActivityTab();
            FragmentDrawer.positionbefore=0;
        }
        else if(position==2){
            logout();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle("Fabric's Preservation");
        }
    }

    public void logout(){
//        startActivity(new Intent(getApplicationContext(), Login_Menu.class));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure to logout ?");
        builder.setCancelable(false);

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(MainMenu.this, LoginActivity.class));
                FragmentDrawer.adapter.selected_item = 0;

                sharedpreferences = getSharedPreferences(name, mode);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("username", "");
                editor.putString("password", "");
                editor.putString("nama", "");
                editor.putString("email", "");
                editor.putString("alamat", "");
                editor.putString("points", "0");
                editor.putString("gambar", "");
                editor.putString("no_telp", "");
                editor.putString("sisaRedeem", "");
                editor.putString("currentRedeem", "");


                editor.commit();

                spnama = sharedpreferences.getString("ceknama", "");
                spusername = sharedpreferences.getString("cekusername", "");
                Toast.makeText(MainMenu.this, "Logout Success ", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
                FragmentDrawer.adapter.selected_item = FragmentDrawer.positionbefore;
                FragmentDrawer.recyclerView.getAdapter().notifyDataSetChanged();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}

package com.pelestariankain.plbtw.pelestariankain.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;
import com.pelestariankain.plbtw.pelestariankain.model.ListDataKain;
import com.pelestariankain.plbtw.pelestariankain.model.ListLogUser;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joko Adi Hartono on 6/3/2016.
 */
public class AdapterDataLogUser extends ArrayAdapter<ListLogUser> {
    JSONArray names = null;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    Dialog dialog;

    private TextView txtNama;
    private TextView txtUsername;
    private TextView txtJenis;
    private TextView txtWaktu;
    private Button btnDetil;
    private TextView dialogTxtNama ;
    private TextView dialogTxtJenis ;
    private TextView dialogTxtDeskripsi ;
    private TextView dialogTxtWaktu ;

    private TextView dialogTxtDesa;
    private TextView dialogTxtKecamatan;
    private TextView dialogTxtKabupaten;
    private TextView dialogTxtProvinsi ;

    private ImageView dialogImgGambar;
    private ImageView imageView;

    private String url;
    public Bitmap gambar;
    Context context;
    int resLayout;
    List<ListLogUser> listItems;
    CheckBox checkItem;
    public static ArrayList<Boolean> positionArray;
    public AdapterDataLogUser(Context context, int resLayout, List<ListLogUser> listItems){
        super(context,resLayout,listItems);
        this.context=context;
        this.resLayout=resLayout;
        this.listItems=listItems;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(context,resLayout,null);


        imageView = (ImageView) v.findViewById(R.id.fragment_list_log_user_imageView);
        url = "http://pelestariankain.esy.es/plbtw-rest/images/" + listItems.get(position).getGambar();
        Log.d("", "--------->URL" + url);

        Picasso.with(context).load(url).into(imageView);


        txtNama = (TextView) v.findViewById(R.id.fragment_list_log_user_txtNama);
        txtJenis = (TextView) v.findViewById(R.id.fragment_list_log_user_txtJenis);
//        txtUsername = (TextView) v.findViewById(R.id.fragment_list_log_user_txtUsername);
        txtWaktu = (TextView) v.findViewById(R.id.fragment_list_log_user_txtWaktu);

        btnDetil=(Button) v.findViewById(R.id.fragment_list_log_user_btnDetil);

        txtNama.setText(listItems.get(position).getNama_konten());
        txtJenis.setText(listItems.get(position).getJenis());
        txtWaktu.setText("Time submitted : "+listItems.get(position).getWaktu());
////        Log.d("cek adapter:", listItems.get(position).get("jenis"));
        btnDetil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setContentView(R.layout.detaildialogloguser);
                dialog.setTitle("Detail Fabric");
                dialogTxtNama = (TextView) dialog.findViewById(R.id.detildialogloguser_txtNama);
                dialogTxtJenis = (TextView) dialog.findViewById(R.id.detildialogloguser_txtJenis);
                dialogTxtDeskripsi = (TextView) dialog.findViewById(R.id.detildialogloguser_txtDeskripsi);
                dialogTxtWaktu = (TextView) dialog.findViewById(R.id.detildialogloguser_txtWaktu);
                dialogImgGambar = (ImageView) dialog.findViewById(R.id.detildialogloguser_imgView);


                dialogTxtDesa = (TextView) dialog.findViewById(R.id.detildialogloguser_txtDesa);
                dialogTxtKecamatan = (TextView) dialog.findViewById(R.id.detildialogloguser_txtKecamatan);
                dialogTxtKabupaten = (TextView) dialog.findViewById(R.id.detildialogloguser_txtKabupaten);
                dialogTxtProvinsi = (TextView) dialog.findViewById(R.id.detildialogloguser_txtProvinsi);

                Button dialogBtnClose = (Button) dialog.findViewById(R.id.detildialogloguser_btnClose);
                dialogTxtNama.setText(listItems.get(position).getNama_konten());
                dialogTxtJenis.setText(listItems.get(position).getJenis());
                dialogTxtDeskripsi.setText(listItems.get(position).getDeskripsi());
                dialogTxtDesa.setText("Village: " + listItems.get(position).getDesa());
                dialogTxtKecamatan.setText("Sub-district: " + listItems.get(position).getKecamatan());
                dialogTxtKabupaten.setText("District: " + listItems.get(position).getKabupaten());
                dialogTxtProvinsi.setText("Province: " + listItems.get(position).getProvinsi());
                dialogTxtWaktu.setText("Time Submitted: " + listItems.get(position).getWaktu());
                url = "http://pelestariankain.esy.es/plbtw-rest/images/" + listItems.get(position).getGambar();
                Picasso.with(context).load(url).into(dialogImgGambar);


                dialogBtnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
        return v;
    }
}

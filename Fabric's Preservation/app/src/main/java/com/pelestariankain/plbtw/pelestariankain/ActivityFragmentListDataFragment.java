package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterDataKain;
import com.pelestariankain.plbtw.pelestariankain.model.ListDataKain;
import com.pelestariankain.plbtw.pelestariankain.tab.SlidingTabLayout;
import com.pelestariankain.plbtw.pelestariankain.user.RegisterActivity;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentListDataFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String URL_READ="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/kontenall";


    ProgressDialog pDialog;
    final Context context = getActivity();

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    private SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ListDataKain> arrayListKain;
    public Bitmap gambar;
    private Dialog dialog;
    private FloatingActionButton btnTambah;
    private ListView listKain;
    public static ListAdapter adapter;

    public ActivityFragmentListDataFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_activity_fragment_list_data, container, false);
        arrayListKain = new ArrayList<ListDataKain>();
        listKain = (ListView) v.findViewById(R.id.listView);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        btnTambah=(FloatingActionButton)v.findViewById(R.id.fragment_list_data_float_btnTambah);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),AddData.class));
            }
        });
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        if(arrayListKain==null) {
            new readKainJSON().execute();
        }else{
            pDialog = new ProgressDialog(getActivity());
        }
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(pDialog.isShowing())
                                            pDialog.dismiss();
                                        swipeRefreshLayout.setRefreshing(true);
                                        new readKainJSON().execute();
                                    }
                                }
        );
        dialog = new Dialog(getActivity());

        return v;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity)getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public void onRefresh() {
        new readKainJSON().execute();
    }


    class readKainJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status,id_pegawai;

        public readKainJSON()
        {
            this.id_pegawai=id_pegawai;
            this.status=status;
            if(arrayListKain==null) {
                pDialog = new ProgressDialog(getActivity());
            }
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        //    if(arrayListKain==null) {
                pDialog.setMessage("Reading data fabricsss.. Please Wait...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
           // }
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);

                if(json!=null){
                    sukses=json.getString("status");
                    Log.d("test : ", json.toString());
                    Log.d("test : ", json.getString("kontenData"));
                    Log.d("test : ", json.getJSONArray("kontenData").toString());

                    if(sukses.equalsIgnoreCase("1")){
                        if(json!=null){
                            Log.d("test : ", json.toString());
                            arrayListKain = new ArrayList<ListDataKain>();
                            jsonarray = json.getJSONArray("kontenData");
                            Log.d("test : ", json.toString());

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String username = jsonobject.getString("username");
                                String nama = jsonobject.getString("nama_konten");
                                String jenis= jsonobject.getString("jenis");
                                String deskripsi= jsonobject.getString("deskripsi");
                                String desa= jsonobject.getString("desa");
                                String kecamatan= jsonobject.getString("kecamatan");
                                String kabupaten= jsonobject.getString("kabupaten");
                                String provinsi= jsonobject.getString("provinsi");
                                String gambar= jsonobject.getString("gambar");
                                arrayListKain.add(new ListDataKain(username,nama,jenis,deskripsi,desa,kecamatan,kabupaten,provinsi,gambar));
                            }

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;

        }

        //output
        @Override
        protected void onPostExecute(String file_url){
            if(pDialog!=null) {
                pDialog.dismiss();
            }
            if(sukses.equalsIgnoreCase("1")){
                adapter = new AdapterDataKain(getActivity(),
                        R.layout.list_kain,arrayListKain);
                listKain.setAdapter(adapter);
            }
            swipeRefreshLayout.setRefreshing(false);

        }
    }


}

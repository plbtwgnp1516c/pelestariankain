package com.pelestariankain.plbtw.pelestariankain.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;
import com.pelestariankain.plbtw.pelestariankain.model.ListLogRedeem;
import com.pelestariankain.plbtw.pelestariankain.model.ListLogUser;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joko Adi Hartono on 6/3/2016.
 */
public class AdapterDataLogRedeem extends ArrayAdapter<ListLogRedeem> {
    JSONArray names = null;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    Dialog dialog;

    private TextView txtNama;
    private TextView txtPointNeeded;
    private TextView txtWaktu;

    private Button btnDetil;
    private TextView dialogTxtNama ;
    private TextView dialogTxtDeskripsi ;
    private TextView dialogTxtWaktu ;
    private TextView dialogTxtPointNeeded;


    private ImageView dialogImgGambar;
    private ImageView imageView;

    private String url;
    public Bitmap gambar;
    Context context;
    int resLayout;
    List<ListLogRedeem> listItems;
    CheckBox checkItem;
    public static ArrayList<Boolean> positionArray;
    public AdapterDataLogRedeem(Context context, int resLayout, List<ListLogRedeem> listItems){
        super(context,resLayout,listItems);
        this.context=context;
        this.resLayout=resLayout;
        this.listItems=listItems;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(context,resLayout,null);

        imageView = (ImageView) v.findViewById(R.id.fragment_list_log_redeem_imageView);
        url = "http://pelestariankain.esy.es/plbtw-rest/rewards/" + listItems.get(position).getGambar();
        Log.d("", "--------->URL" + url);

        Picasso.with(context).load(url).into(imageView);

        txtNama = (TextView) v.findViewById(R.id.fragment_list_log_redeem_txtNama);
        txtPointNeeded = (TextView) v.findViewById(R.id.fragment_list_log_redeem_txtPointNeeded);
        txtWaktu = (TextView) v.findViewById(R.id.fragment_list_log_redeem_txtWaktu);
        btnDetil=(Button) v.findViewById(R.id.fragment_list_log_redeem_btnDetil);
        txtNama.setText(listItems.get(position).getNama_reward());
        txtPointNeeded.setText("Point Needed: "+listItems.get(position).getPoint_needed());
        txtWaktu.setText("Time submitted : "+listItems.get(position).getWaktu());
        btnDetil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setContentView(R.layout.detaildialoglogredeem);
                dialog.setTitle("Detail Redeem");
                dialogTxtNama = (TextView) dialog.findViewById(R.id.detildialoglogredeem_txtNama);
                dialogTxtDeskripsi = (TextView) dialog.findViewById(R.id.detildialoglogredeem_txtDeskripsi);
                dialogTxtWaktu = (TextView) dialog.findViewById(R.id.detildialoglogredeem_txtWaktu);
                dialogImgGambar = (ImageView) dialog.findViewById(R.id.detildialoglogredeem_imgView);
                dialogTxtPointNeeded = (TextView) dialog.findViewById(R.id.detildialoglogredeem_txtPointNeeded);

                Button dialogBtnClose = (Button) dialog.findViewById(R.id.detildialoglogredeem_btnClose);
                dialogTxtNama.setText(listItems.get(position).getNama_reward());
                dialogTxtDeskripsi.setText(listItems.get(position).getDeskripsi());
                dialogTxtPointNeeded.setText("Point Needed: " + listItems.get(position).getPoint_needed());
                dialogTxtWaktu.setText("Time Submitted: " + listItems.get(position).getWaktu());
                url = "http://pelestariankain.esy.es/plbtw-rest/rewards/" + listItems.get(position).getGambar();

                Picasso.with(context).load(url).into(dialogImgGambar);
                dialogBtnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return v;
    }
}

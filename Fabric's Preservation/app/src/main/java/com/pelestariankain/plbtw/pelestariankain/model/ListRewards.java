package com.pelestariankain.plbtw.pelestariankain.model;

/**
 * Created by Joko Adi Hartono on 6/3/2016.
 */
public class ListRewards {
    private String NamaReward;
    private String Deskripsi;
    private int PointNeeeded;
    private String Gambar;
    private int Stock;
    private int CurrentRedeem;
    private int id_reward;


    public ListRewards(int id_reward, String NamaReward,String Deskripsi, int PointNeeded, String Gambar, int Stock, int CurrentRedeem){
        setDeskripsi(Deskripsi);
        setNamaReward(NamaReward);
        setPointNeeeded(PointNeeded);
        setGambar(Gambar);
        setStock(Stock);
        setCurrentRedeem(CurrentRedeem);
        setId_reward(id_reward);
    }

    public String getNamaReward() {
        return NamaReward;
    }

    public void setNamaReward(String namaReward) {
        NamaReward = namaReward;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public int getPointNeeeded() {
        return PointNeeeded;
    }

    public void setPointNeeeded(int pointNeeeded) {
        PointNeeeded = pointNeeeded;
    }

    public String getGambar() {
        return Gambar;
    }

    public void setGambar(String gambar) {
        Gambar = gambar;
    }


    public int getCurrentRedeem() {
        return CurrentRedeem;
    }

    public void setCurrentRedeem(int currentRedeem) {
        CurrentRedeem = currentRedeem;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int stock) {
        Stock = stock;
    }

    public int getId_reward() {
        return id_reward;
    }

    public void setId_reward(int id_reward) {
        this.id_reward = id_reward;
    }
}

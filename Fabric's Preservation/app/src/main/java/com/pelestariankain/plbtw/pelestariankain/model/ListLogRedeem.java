package com.pelestariankain.plbtw.pelestariankain.model;

/**
 * Created by Joko Adi Hartono on 6/6/2016.
 */
public class ListLogRedeem {
    private String nama_reward;
    private String deskripsi;
    private String point_needed;
    private String gambar;
    private String username;
    private String waktu;
    public ListLogRedeem(String nama_reward, String deskripsi, String point_needed, String gambar, String username, String waktu){
        setNama_reward(nama_reward);
        setDeskripsi(deskripsi);
        setWaktu(waktu);
        setGambar(gambar);
        setPoint_needed(point_needed);
        setUsername(username);
    }

    public String getNama_reward() {
        return nama_reward;
    }

    public void setNama_reward(String nama_reward) {
        this.nama_reward = nama_reward;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getPoint_needed() {
        return point_needed;
    }

    public void setPoint_needed(String point_needed) {
        this.point_needed = point_needed;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }
}

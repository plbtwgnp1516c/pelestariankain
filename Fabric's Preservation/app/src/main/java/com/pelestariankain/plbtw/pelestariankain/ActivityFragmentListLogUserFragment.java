package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterDataKain;
import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterDataLogUser;
import com.pelestariankain.plbtw.pelestariankain.model.ListDataKain;
import com.pelestariankain.plbtw.pelestariankain.model.ListLogUser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentListLogUserFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {
    private static final String URL_READ="http://pelestariankain.esy.es/plbtw-rest/index.php/private/user/loguser";

    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;


    ProgressDialog pDialog;
    final Context context = getActivity();

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    private String id_user;

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    private SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ListLogUser> arrayListLogUser;
    public Bitmap gambar;
    private Dialog dialog;
    private ListView listLogUser;
    public static ListAdapter adapter;

    public ActivityFragmentListLogUserFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_activity_fragment_list_log_user, container, false);
        sharedpreferences=getActivity().getSharedPreferences(name,mode);
        id_user=sharedpreferences.getString("id_user", "");
        arrayListLogUser = new ArrayList<ListLogUser>();
        listLogUser = (ListView) v.findViewById(R.id.fragment_log_user_listView);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        if(arrayListLogUser==null) {
            new readLogUserJSON().execute();
        }else{
            pDialog = new ProgressDialog(getActivity());
        }
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(pDialog.isShowing())
                                            pDialog.dismiss();

                                        swipeRefreshLayout.setRefreshing(true);
                                        new readLogUserJSON().execute();
                                    }
                                }
        );
        dialog = new Dialog(getActivity());

        return v;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity)getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public void onRefresh() {
        new readLogUserJSON().execute();
    }

    class readLogUserJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status,id_pegawai;

        public readLogUserJSON()
        {
            this.id_pegawai=id_pegawai;
            this.status=status;
            if(arrayListLogUser==null) {
                pDialog = new ProgressDialog(getActivity());
            }
        }

        protected  void onPreExecute(){
            super.onPreExecute();
      //      if(arrayListLogUser==null) {
                pDialog.setMessage("Reading data fabricsss.. Please Wait...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
//            }
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_user",id_user));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);

                if(json!=null){
                    sukses=json.getString("status");
//                    Log.d("test : ", json.toString());
//                    Log.d("test : ", json.getString("userData"));
//                    Log.d("test : ", json.getJSONArray("userData").toString());

                    if(sukses.equalsIgnoreCase("1")){
                        if(json!=null){
                            Log.d("test : ", json.toString());
                            arrayListLogUser = new ArrayList<ListLogUser>();
                            jsonarray = json.getJSONArray("userData");
                            Log.d("test : ", json.toString());

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String username = jsonobject.getString("username");
                                String nama = jsonobject.getString("nama_konten");
                                String jenis= jsonobject.getString("jenis");
                                String deskripsi= jsonobject.getString("deskripsi");
                                String desa= jsonobject.getString("desa");
                                String kecamatan= jsonobject.getString("kecamatan");
                                String kabupaten= jsonobject.getString("kabupaten");
                                String provinsi= jsonobject.getString("provinsi");
                                String gambar= jsonobject.getString("gambar");
                                String waktu= jsonobject.getString("waktu");

                                arrayListLogUser.add(new ListLogUser(nama,jenis,deskripsi,desa,kecamatan,kabupaten,provinsi,gambar,username,waktu));
                            }

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;

        }

        //output
        @Override
        protected void onPostExecute(String file_url){
            if(pDialog!=null) {
                pDialog.dismiss();
            }
            if(sukses.equalsIgnoreCase("1")){
                adapter = new AdapterDataLogUser(getActivity(),
                        R.layout.list_log_user,arrayListLogUser);
                listLogUser.setAdapter(adapter);
            }
            swipeRefreshLayout.setRefreshing(false);

        }
    }


}

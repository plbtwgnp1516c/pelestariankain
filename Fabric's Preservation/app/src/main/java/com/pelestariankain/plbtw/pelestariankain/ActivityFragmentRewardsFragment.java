package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterReward;
import com.pelestariankain.plbtw.pelestariankain.model.ListRewards;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentRewardsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String URL_READ="http://pelestariankain.esy.es/plbtw-rest/index.php/private/pelestariankain/GetRewards";


    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private AdapterReward adapter;
    private ArrayList<ListRewards> arrayListRewards;
    private Dialog dialog;
    private GridView gridRewards;
    private TextView txtNama;
    private TextView txtDeskripsi;
    private TextView txtRedeemRemaining;
    private String redeemRemaining;

    private TextView txtPoint;
    private SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog pDialog;

    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;


    public Bitmap gambar;

    public ActivityFragmentRewardsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_activity_fragment_rewards, container, false);
        gridRewards=(GridView)v.findViewById(R.id.fragment_rewards_gridView);
        sharedpreferences=getActivity().getSharedPreferences(name,mode);
        redeemRemaining=sharedpreferences.getString("sisaRedeem", "");
        txtRedeemRemaining=(TextView)v.findViewById(R.id.fragment_rewards_redeemremaining);
        txtRedeemRemaining.setText("You have "+sharedpreferences.getString("sisaRedeem","")+" times remaining to redeem items");

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.fragment_rewards_swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        if(arrayListRewards==null) {
            new readRewardJSON().execute();
        }
        else
            pDialog = new ProgressDialog(getActivity());

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (pDialog.isShowing())
                                            pDialog.dismiss();
                                        swipeRefreshLayout.setRefreshing(true);
                                        new readRewardJSON().execute();
                                    }
                                }
        );

        return v;
    }
    @Override
    public void onRefresh() {
        txtRedeemRemaining.setText("You have "+sharedpreferences.getString("sisaRedeem","")+" times remaining to redeem items");
        new readRewardJSON().execute();
    }
    class readRewardJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status,id_pegawai;

        public readRewardJSON()
        {
            this.id_pegawai=id_pegawai;
            this.status=status;
            if(arrayListRewards==null)
            pDialog = new ProgressDialog(getActivity());

        }

        protected  void onPreExecute() {
            super.onPreExecute();
       //     if (arrayListRewards == null) {
                pDialog.setMessage("Reading data rewards.. Please Wait...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
         //   }
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                if(json!=null){
                    sukses=json.getString("status");
                    Log.d("test : ", json.toString());
//                    Log.d("test : ", json.getString("kontenData"));
//                    Log.d("test : ", json.getJSONArray("kontenData").toString());

                    if(sukses.equalsIgnoreCase("1")){
                        if(json!=null){
                            Log.d("test : ", json.toString());
                            arrayListRewards = new ArrayList<ListRewards>();
                            jsonarray = json.getJSONArray("rewardsData");
                            Log.d("test : ", json.toString());
                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String id_reward = jsonobject.getString("id_reward");
                                String nama = jsonobject.getString("nama_reward");
                                String point_needed= jsonobject.getString("point_needed");
                                String deskripsi= jsonobject.getString("deskripsi");
                                String gambar= jsonobject.getString("gambar");
                                String stock= jsonobject.getString("stock");
                                String currentredeem= jsonobject.getString("currentredeem");
                                arrayListRewards.add(new ListRewards(Integer.parseInt(id_reward),nama,deskripsi,Integer.parseInt(point_needed),
                                        gambar,Integer.parseInt(stock),Integer.parseInt(currentredeem)));
                            }

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;

        }

        //output
        protected void onPostExecute(String file_url){
            if(sukses.equalsIgnoreCase("1")){
//                Log.d("ARRAYY", "ARRAYY --------->" + arrayListRewards.get(0).getDeskripsi());
                adapter = new AdapterReward(getActivity(),
                            R.layout.list_rewards,arrayListRewards);
                gridRewards.setAdapter(adapter);


            }
            if(pDialog!=null)
                pDialog.dismiss();
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}

package com.pelestariankain.plbtw.pelestariankain.model;

/**
 * Created by Joko Adi Hartono on 6/3/2016.
 */
public class ListDataKain {
    private String Username;
    private String Nama;
    private String Jenis;
    private String Deskripsi;
    private String Desa;
    private String Kecamatan;
    private String Kabupaten;
    private String Provinsi;
    private String Gambar;



    public ListDataKain(String Username, String Nama, String Jenis, String Deskripsi, String Desa, String Kecamatan, String Kabupaten, String Provinsi, String Gambar){
        setDeskripsi(Deskripsi);
        setDesa(Desa);
        setGambar(Gambar);
        setJenis(Jenis);
        setKabupaten(Kabupaten);
        setKecamatan(Kecamatan);
        setNama(Nama);
        setProvinsi(Provinsi);
        setUsername(Username);
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getJenis() {
        return Jenis;
    }

    public void setJenis(String jenis) {
        Jenis = jenis;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getDesa() {
        return Desa;
    }

    public void setDesa(String desa) {
        Desa = desa;
    }

    public String getKecamatan() {
        return Kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        Kecamatan = kecamatan;
    }

    public String getKabupaten() {
        return Kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        Kabupaten = kabupaten;
    }

    public String getProvinsi() {
        return Provinsi;
    }

    public void setProvinsi(String provinsi) {
        Provinsi = provinsi;
    }

    public String getGambar() {
        return Gambar;
    }

    public void setGambar(String gambar) {
        Gambar = gambar;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }
}

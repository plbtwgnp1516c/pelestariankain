package com.pelestariankain.plbtw.pelestariankain.model;

/**
 * Created by Joko Adi Hartono on 6/6/2016.
 */
public class ListLogUser {
    private String nama_konten;
    private String jenis;
    private String deskripsi;
    private String desa;
    private String kecamatan;
    private String kabupaten;
    private String provinsi;
    private String gambar;
    private String username;
    private String waktu;
    public ListLogUser(String nama_konten, String jenis, String deskripsi,String desa, String kecamatan,
                       String kabupaten, String provinsi, String gambar, String username, String waktu){
        setUsername(username);
        setDesa(desa);
        setDeskripsi(deskripsi);
        setGambar(gambar);
        setJenis(jenis);
        setKabupaten(kabupaten);
        setProvinsi(provinsi);
        setKecamatan(kecamatan);
        setNama_konten(nama_konten);
        setWaktu(waktu);

    }

    public String getNama_konten() {
        return nama_konten;
    }

    public void setNama_konten(String nama_konten) {
        this.nama_konten = nama_konten;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getDesa() {
        return desa;
    }

    public void setDesa(String desa) {
        this.desa = desa;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }
}

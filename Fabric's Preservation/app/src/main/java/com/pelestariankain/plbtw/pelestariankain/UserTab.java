package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.pelestariankain.plbtw.pelestariankain.NavDrawer.FragmentDrawer;
import com.pelestariankain.plbtw.pelestariankain.tab.SlidingTabLayout;

//import com.martabakmobile.mobileadmin.Adpater.AdapterMartabak;
//import com.example.jokoadihartono.mobileadmin.tab;

//import com.example.jokoadihartono.tab.SlidingTabLayout;

//import it.neokree.materialtabs.MaterialTab;
//import it.neokree.materialtabs.MaterialTabHost;
//import it.neokree.materialtabs.MaterialTabListener;

public class UserTab extends Fragment {

    Toolbar toolbar;
    ViewPager pager;
    ViewPagerUserAdapter adapter;
    private SlidingTabLayout tabs;
    CharSequence Titles[]={"User Info","User Log","Redeem Log"};
    int Numboftabs =3;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_activity_user, container, false);
        toolbar=(Toolbar)v.findViewById(R.id.toolbar);
//        v.setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerUserAdapter(getActivity().getSupportFragmentManager(),Titles,Numboftabs);
        getActivity().setResult(getActivity().RESULT_OK);
        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) v.findViewById(R.id.tab_user_viewPager);
//        adapter.notifyDataSetChanged();
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) v.findViewById(R.id.tab_user_tab);

        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorTab);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout

        adapter.notifyDataSetChanged();
        tabs.setViewPager(pager);
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
//                ActivityFragmentListDataFragment.adapter.notifyDataSetChanged();
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}

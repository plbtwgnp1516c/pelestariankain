package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterDataLogRedeem;
import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterDataLogUser;
import com.pelestariankain.plbtw.pelestariankain.model.ListLogRedeem;
import com.pelestariankain.plbtw.pelestariankain.model.ListLogUser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentListLogRedeemFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {
    private static final String URL_READ="http://pelestariankain.esy.es/plbtw-rest/index.php/private/redeem/logredeem";

    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;


    ProgressDialog pDialog;
    final Context context = getActivity();

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    private String id_user;

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    private SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ListLogRedeem> arrayListLogRedeem;
    public Bitmap gambar;
    private Dialog dialog;
    private ListView listLogRedeem;
    public static ListAdapter adapter;

    public ActivityFragmentListLogRedeemFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_activity_fragment_list_log_user, container, false);
        sharedpreferences=getActivity().getSharedPreferences(name,mode);
        id_user=sharedpreferences.getString("id_user", "");
        arrayListLogRedeem = new ArrayList<ListLogRedeem>();
        listLogRedeem = (ListView) v.findViewById(R.id.fragment_log_user_listView);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        if(arrayListLogRedeem==null) {
            new readLogRedeemJSON().execute();
        }else{
            pDialog = new ProgressDialog(getActivity());
        }
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(pDialog.isShowing())
                                            pDialog.dismiss();
                                        swipeRefreshLayout.setRefreshing(true);
                                        new readLogRedeemJSON().execute();
                                    }
                                }
        );
        dialog = new Dialog(getActivity());

        return v;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity)getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    @Override
    public void onRefresh() {
        new readLogRedeemJSON().execute();
    }

    class readLogRedeemJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status,id_pegawai;

        public readLogRedeemJSON()
        {
            this.id_pegawai=id_pegawai;
            this.status=status;
            if(arrayListLogRedeem==null) {
                pDialog = new ProgressDialog(getActivity());
            }
        }

        protected  void onPreExecute(){
            super.onPreExecute();
         //   if(arrayListLogRedeem==null) {
                pDialog.setMessage("Reading data fabricsss.. Please Wait...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
         //   }
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_user",id_user));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);

                if(json!=null){
                    sukses=json.getString("status");
//                    Log.d("test : ", json.toString());
//                    Log.d("test : ", json.getString("redeemData"));
//                    Log.d("test : ", json.getJSONArray("redeemData").toString());

                    if(sukses.equalsIgnoreCase("1")){
                        if(json!=null){
                            Log.d("test : ", json.toString());
                            arrayListLogRedeem = new ArrayList<ListLogRedeem>();
                            jsonarray = json.getJSONArray("redeemData");
                            Log.d("test : ", json.toString());

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String username = jsonobject.getString("username");
                                String nama = jsonobject.getString("nama_reward");
                                String deskripsi= jsonobject.getString("deskripsi");
                                String point_needed= jsonobject.getString("point_needed");
                                String gambar= jsonobject.getString("gambar");
                                String waktu= jsonobject.getString("waktu");
                                arrayListLogRedeem.add(new ListLogRedeem(nama,deskripsi,point_needed,gambar,username,waktu));
                            }

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;

        }

        //output
        @Override
        protected void onPostExecute(String file_url){
            if(pDialog!=null) {
                pDialog.dismiss();
            }
            if(sukses.equalsIgnoreCase("1")){

                adapter = new AdapterDataLogRedeem(getActivity(),
                        R.layout.list_log_redeem,arrayListLogRedeem);
                listLogRedeem.setAdapter(adapter);
            }
            swipeRefreshLayout.setRefreshing(false);

        }
    }


}

package com.pelestariankain.plbtw.pelestariankain.user;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.pelestariankain.plbtw.pelestariankain.ActivityTab;
import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GantiPasswordActivity extends AppCompatActivity {

    private static final String URL_CREATE="http://xiaokwee.5gbfree.com/MartabakMobile/user_controller/update_password.php";
    //private static final String URL_CREATE="http://localhost/privalet_apps/user_login.php";

    private SharedPreferences sharedpreferences;
    private final String name="myAccount";
    private static final int mode = Activity.MODE_PRIVATE;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    EditText txtPasswordLama,txtPasswordBaru,txtrePasswordBaru;
    Button btnGanti;

    String password_lama,username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_password);

        txtPasswordLama = (EditText) findViewById(R.id.txtPasswordLama1);
        txtPasswordBaru = (EditText) findViewById(R.id.txtPasswordBaru1);
        txtrePasswordBaru = (EditText) findViewById(R.id.txtRePasswordBaru1);

        sharedpreferences= getSharedPreferences(name, mode);
        password_lama=sharedpreferences.getString("password", "");
        username=sharedpreferences.getString("username", "");

        btnGanti = (Button) findViewById(R.id.btnGanti);
    }


    public void btnGanti_onClick(View v){
        if(check())
        {
            new gantiPasswordJSOn(username,txtPasswordBaru.getText().toString()).execute();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("password", txtPasswordBaru.getText().toString());
            editor.apply();
            finish();
            Toast.makeText(getApplicationContext(),"Password telah terganti :D",Toast.LENGTH_LONG).show();
        }
    }

    public boolean check(){
        if(txtPasswordLama.getText().toString().trim().isEmpty()||
           txtPasswordBaru.getText().toString().trim().isEmpty()||
           txtrePasswordBaru.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(),"Masukkan data yang benar",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!txtPasswordLama.getText().toString().equalsIgnoreCase(password_lama)) {
            Toast.makeText(getApplicationContext(), "Masukkan password lama yang benar", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!txtPasswordBaru.getText().toString().equalsIgnoreCase(txtrePasswordBaru.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Masukkan password baru yang benar", Toast.LENGTH_LONG).show();
            return false;
        }
        else
            return true;
    }


    class gantiPasswordJSOn extends AsyncTask<String, String, String> {
        String sukses = "";
        String username,password;

        public gantiPasswordJSOn(String username,String password){
            this.username=username;
            this.password=password;
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username",username));
            params.add(new BasicNameValuePair("password",password));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_CREATE,"GET",params);
                Log.d("", "-------->URL:"+json.toString());
                //Log.d("", "-------->Data:"+json.toString());
                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            if(sukses.equalsIgnoreCase("berhasil")){
                finish();
            }
        }
    }
}

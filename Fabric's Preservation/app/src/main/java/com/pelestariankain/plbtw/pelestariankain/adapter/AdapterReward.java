package com.pelestariankain.plbtw.pelestariankain.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.model.ListRewards;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.pelestariankain.plbtw.pelestariankain.R;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Joko Adi Hartono on 1/7/2016.
 */
public class AdapterReward extends ArrayAdapter<ListRewards> {

    private static final String URL_CREATE="http://pelestariankain.esy.es/plbtw-rest/index.php/private/redeem/addredeem";


    private TextView txtNama;
    private TextView txtDeskripsi;
    private TextView txtPoint;
    private TextView dialogTxtNama;
    private TextView dialogTxtDeskripsi;
    private TextView dialogTxtPoint;
    private TextView dialogTxtStock;
    private ImageView dialogImgGambar;
    private Button dialogBtnRedeem;

    private Button btnDetail;
    private Button dialogBtnClose;
    private Dialog dialog;
    private String url;
    private ImageView imageView;
    private ProgressDialog pDialog;
    private SharedPreferences sharedpreferences;
    private final String name = "User";
    public static final int mode = Activity.MODE_PRIVATE;

    private String username;
    private String id_user;
    private String nama;
    private String email;
    private String alamat;
    private String noTelp;
    private String points;
    private String gambar;
    private String sisaRedeem;
    private String currentRedeem;


    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    Context context;
    int resLayout;
    List<ListRewards> listItems;
    CheckBox checkItem;
    public static ArrayList<Boolean> positionArray;
    public AdapterReward(Context context, int resLayout, List<ListRewards> listItems){
        super(context,resLayout,listItems);
        this.context=context;
        this.resLayout=resLayout;
        this.listItems=listItems;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = View.inflate(context,resLayout,null);

        sharedpreferences=getContext().getSharedPreferences(name,mode);


        txtNama= (TextView)v.findViewById(R.id.fragment_listrewards_txtNama);
        imageView = (ImageView) v.findViewById(R.id.fragment_listrewards_ImgView);
        url = "http://pelestariankain.esy.es/plbtw-rest/rewards/" + listItems.get(position).getGambar();
        Log.d("", "--------->URL" + url);
        Picasso.with(context).load(url).into(imageView);

        txtPoint= (TextView)v.findViewById(R.id.fragment_listrewards_txtPoints);
        btnDetail=(Button)v.findViewById(R.id.fragment_listrewards_btnRedeem);

        final ListRewards listItem = listItems.get(position);


        username=sharedpreferences.getString("username","");
        id_user=sharedpreferences.getString("id_user","");
        nama=sharedpreferences.getString("password", "");
        email=sharedpreferences.getString("email","");
        alamat=sharedpreferences.getString("alamat","");
        noTelp=sharedpreferences.getString("no_telp","");
        points=sharedpreferences.getString("points","");
        gambar=sharedpreferences.getString("gambar","");
        sisaRedeem=sharedpreferences.getString("sisaRedeem","");
        currentRedeem=sharedpreferences.getString("currentRedeem","");

        txtNama.setText(listItem.getNamaReward());
        txtPoint.setText(String.valueOf(listItem.getPointNeeeded()));
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context);
                dialog.setContentView(R.layout.detaildialogrewards);
                dialog.setTitle("Detail Rewards");
                dialogTxtNama = (TextView) dialog.findViewById(R.id.detildialogrewards_txtNama);
                dialogTxtDeskripsi = (TextView) dialog.findViewById(R.id.detildialogrewards_txtDeskripsi);
                dialogTxtPoint = (TextView) dialog.findViewById(R.id.detildialogrewards_txtPoint);
                dialogTxtStock = (TextView) dialog.findViewById(R.id.detildialogrewards_txtStock);
                dialogBtnClose= (Button)dialog.findViewById(R.id.detildialogrewards_btnClose);
                dialogBtnRedeem =(Button)dialog.findViewById(R.id.detildialogrewards_btnRedeem);
                dialogImgGambar= (ImageView)dialog.findViewById(R.id.detildialogrewards_imgView);
                String url2 = "http://pelestariankain.esy.es/plbtw-rest/rewards/" + listItems.get(position).getGambar();
                Picasso.with(context).load(url2).into(dialogImgGambar);
                dialogTxtNama.setText(listItem.getNamaReward());
                dialogTxtDeskripsi.setText(listItem.getDeskripsi());
                dialogTxtPoint.setText(String.valueOf(listItem.getPointNeeeded()));
                dialogTxtStock.setText(String.valueOf(listItem.getStock()));
                dialogBtnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogBtnRedeem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("id user: ", sharedpreferences.getString("id_user", ""));
                        Log.d("id reward: ", String.valueOf(listItem.getId_reward()));
                        if(Integer.parseInt(sisaRedeem)<=0) Toast.makeText(getContext(),"You can't redeem anymore!", Toast.LENGTH_LONG).show();
                        else if(listItem.getStock()==0) Toast.makeText(getContext(),"Stock is empty!", Toast.LENGTH_LONG).show();
                        else if(Integer.parseInt(points)< listItem.getPointNeeeded()) Toast.makeText(getContext(),"You have no enough points to redeem!", Toast.LENGTH_LONG).show();
                        else {
                            listItem.setStock(listItem.getStock()-1);
                            points=String.valueOf(Integer.parseInt(points)- listItem.getPointNeeeded());
                            new createRedeem(Integer.parseInt(id_user), listItem.getId_reward(), listItem.getPointNeeeded()).execute();
                        }
                    }
                });
                dialog.show();


            }
        });
        return v;
    }
    class createRedeem extends AsyncTask<String, String, String> {
        String sukses = "";
        int id_reward,id_user,point_needed;
        public createRedeem(int id_user,int id_reward,int point_needed)
        {
            this.id_reward=id_reward;
            this.id_user=id_user;
            this.point_needed=point_needed;
            pDialog = new ProgressDialog(getContext());

        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Creating.. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_reward",Integer.toString(id_reward)));
            params.add(new BasicNameValuePair("id_user", Integer.toString(id_user)));
            params.add(new BasicNameValuePair("point_needed", Integer.toString(point_needed)));
            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "POST", params);
                if (json != null) {
                    sukses = json.getString("status");
                    if (sukses.equalsIgnoreCase("1")) {
                        if (json != null) {
                            Log.d("json: ", "test");
                        }
                    }
                }}catch(JSONException e){
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url){
            if(sukses.equalsIgnoreCase("1")){

                int sisa= Integer.parseInt(sisaRedeem)-1;
                int current= Integer.parseInt(currentRedeem)+1;
                sisaRedeem=String.valueOf(sisa);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("sisaRedeem", sisaRedeem);
                editor.putString("points", points);
                editor.putString("currentRedeem", String.valueOf(current));
                editor.apply();
                dialog.dismiss();
            }
            pDialog.dismiss();
        }
    }
//    public boolean checkKosong(List<ListRewards> listItems){
//        for(int i =0;i<listItems.size();i++){
//           if(positionArray.get(i)==true){
//               return false;
//           }
//        }
//        return true;
//    }
}

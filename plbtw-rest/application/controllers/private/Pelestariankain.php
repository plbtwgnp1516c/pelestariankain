<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Pelestariankain extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Usermodel');
        $this->load->model('Kontenmodel');
        $this->load->model('Rewardsmodel');

    }

    public function redeemChallenge_get(){
        $email = $this->get('username');
        $addpoints = $this->get('addpoints');
        // $addmedal = $this->get('addmedal');
        
        $userData = $this->Usermodel->RedeemChallenge($email,$addpoints);
        
        if($userData != NULL){
            $message =[
                'userData' => $userData,
                'status' => 1,
                'message' => 'Point(s) Added',
            ];
        }else{
            $message =[
                'userData' => 'Point not Added',
                'status' => 0,
                'message' => 'User Not Found',
            ];
        }
        
        $this->set_response($message,REST_Controller::HTTP_CREATED);
    }
    public function kontenAll_get(){
        
        $kontenData = $this->Kontenmodel->GetKonten();
        
        if($kontenData != NULL){
            $message = [
                'kontenData' => $kontenData,
                'status' => 1,
                'message' => 'All Konten Data Received',
            ];
        }else{
            $message = [
                'kontenData' => 'Konten not Found',
                'status' => 0,
                'message' => 'Konten Data Retrieve Failed',
            ];
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
     public function kontenProvinsi_get(){
        
        $kontenData = $this->Kontenmodel->GetKontenProvinsi();
        
        if($kontenData != NULL){
            $message = [
                'kontenData' => $kontenData,
                'status' => 1,
                'message' => 'All Konten Data Received',
            ];
        }else{
            $message = [
                'kontenData' => 'Konten not Found',
                'status' => 0,
                'message' => 'Konten Data Retrieve Failed',
            ];
        }
        
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
    public function kontenKabupaten_get(){
        $provinsi_id= $this->get('provinsi_id');
        $kontenData = $this->Kontenmodel->GetKontenKabupaten($provinsi_id);
        
        if($kontenData != NULL){
            $message = [
                'kontenData' => $kontenData,
                'status' => 1,
                'message' => 'All Konten Data Received',
            ];
        }else{
            $message = [
                'kontenData' => 'Konten not Found',
                'status' => 0,
                'message' => 'Konten Data Retrieve Failed',
            ];
        }
        
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
     public function kontenKecamatan_get(){
        $kabupaten_id= $this->get('kabupaten_id');
        $kontenData = $this->Kontenmodel->GetKontenKecamatan($kabupaten_id);
        if($kontenData != NULL){
            $message = [
                'kontenData' => $kontenData,
                'status' => 1,
                'message' => 'All Konten Data Received',
            ];
        }else{
            $message = [
                'kontenData' => 'Konten not Found',
                'status' => 0,
                'message' => 'Konten Data Retrieve Failed',
            ];
        }
        
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
      public function kontenDesa_get(){
        $kecamatan_id= $this->get('kecamatan_id');
        $kontenData = $this->Kontenmodel->GetKontenDesa($kecamatan_id);
        if($kontenData != NULL){
            $message = [
                'kontenData' => $kontenData,
                'status' => 1,
                'message' => 'All Konten Data Received',
            ];
        }else{
            $message = [
                'kontenData' => 'Konten not Found',
                'status' => 0,
                'message' => 'Konten Data Retrieve Failed',
            ];
        }
        
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }
    public function kontenAdd_get(){
        $nama_konten = $this->get('nama_konten'); // Automatically generated by the model
        $jenis = $this->get('jenis'); // Automatically generated by the model
        $deskripsi = $this->get('deskripsi'); // Automatically generated by the model
        // $desa = $this->post('desa'); // Automatically generated by the model
        // $kecamatan = $this->post('kecamatan'); // Automatically generated by the model
        // $kabupaten = $this->post('kabupaten'); // Automatically generated by the model
        // $provinsi = $this->post('provinsi'); // Automatically generated by the model
        // $this->some_model->update_user( ... );
        $message = [
                'status' => 1,
                'message' => 'Added a resource'
        ];
        $this->Kontenmodel->AddKonten($nama_konten,$jenis,$deskripsi);
        // $this->Kontenmodel->AddKonten($nama,$jenis,$deskripsi,$desa,$kecamatan,$kabupaten,$provinsi);
        
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
     }
    public function kontenAdd_post()
    {

        $nama_konten = $this->post('nama_konten'); // Automatically generated by the model
        $jenis = $this->post('jenis'); // Automatically generated by the model
        $deskripsi = $this->post('deskripsi'); // Automatically generated by the model
        $desa = $this->post('desa'); // Automatically generated by the model
        $kecamatan = $this->post('kecamatan'); // Automatically generated by the model
        $kabupaten = $this->post('kabupaten'); // Automatically generated by the model
        $provinsi = $this->post('provinsi'); // Automatically generated by the model
        // $this->some_model->update_user( ... );
        $message = [
                'status' => 1,
                'message' => 'Added a resource'
        ];
        $this->Kontenmodel->AddKonten($nama_konten,$jenis,$deskripsi,$desa,$kecamatan,$kabupaten,$provinsi);
        // $this->Kontenmodel->AddKonten($nama,$jenis,$deskripsi,$desa,$kecamatan,$kabupaten,$provinsi);
        
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
    public function users_post()
    {
        // $this->some_model->update_user( ... );
        $message = [
            'id' => 100, // Automatically generated by the model
            'name' => $this->post('name'),
            'email' => $this->post('email'),
            'message' => 'Added a resource'
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }
    public function users_get()
    {
        // Users from a data store e.g. database
        $users = [
            [ 'name' => 123, 'id' => 1,'email' => 'john@example.com', 'fact' => 'Loves coding'],
            [ 'name' => 234, 'id' => 2,'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['name' => 345,'id' => 3, 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');
 // var_dump($this->get('id'));
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
            // var_dump($users);

                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <=0)
        {

            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retreival.
        // Usually a model is to be used for this.

        $user = NULL;

        if (!empty($users))
        {
            foreach ($users as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $user = $value;
                }
            }
        }

        if (!empty($user))
        {

            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function redeemRewards_get(){
        $username = $this->get('username');
        $minpoints = $this->get('minpoints');
        
        $userData = $this->Usermodel->RedeemRewards($username,$minpoints);
        
        if($userData != NULL && $minpoints>0){
            $message =[
                'userData' => $userData,
                'status' => 1,
                'message' => 'Succesfully Redeem Reward',
            ];
        }else if($userData == NULL || $minpoints < 0){
            $message =[
                'userData' => 'failed to redeem',
                'status' => 0,
                'message' => 'Redeem Reward not Success',
            ];
        }
        
        $this->set_response($message,REST_Controller::HTTP_CREATED);
    }
    public function GetRewards_get(){
        
        $RewardsData = $this->Rewardsmodel->GetRewardsInfo();
        
        if($RewardsData != NULL){
            $message = [
                'rewardsData' => $RewardsData,
                'status' => 1,
                'message' => 'All Konten Data Received',
            ];
        }else{
            $message = [
                'rewardsData' => 'Konten not Found',
                'status' => 0,
                'message' => 'Konten Data Retrieve Failed',
            ];
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED);
    }

}

<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Konten_model extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function AddKonten($Email, $Nama_Wisata, $Detail_Wisata, $Judul_Konten, $Tipe_Wisata, $Gambar_Wisata, $Tags, $Lokasi)
	{
		$konten = array(
			'Email' => $Email,
			'Nama_Wisata' => $Nama_Wisata,
			'Detail_Wisata' => $Detail_Wisata,
			'Judul_Konten' => $Judul_Konten,
			'Tipe_Wisata' => $Tipe_Wisata,
			'Gambar_Wisata' => $Gambar_Wisata,
			'Tags' => $Tags,
			'Lokasi' => $Lokasi
		);
		$this->db->insert('konten_wisata',$konten);
	}

	function GetKonten($LoggedEmail)
	{
		$this->db->select();
		$this->db->from('konten_wisata');
		
		$query = $this->db->get();
		
		$result =array();
		
		foreach($query->result() as $row) {
			
			$this->db->select()->where('Email', $row->Email);
			$this->db->from('user_app');
			$userquery = $this->db->get();
			
			$this->db->select('flag')->where('Email', $LoggedEmail)->where('ID_Wisata', $row->ID_Wisata);
			$this->db->from('user_votes');
			$flagquery = $this->db->get();
			
			$result[] = array(
			'Email' => $row->Email,
			'Nama_Wisata' => $row->Nama_Wisata,
			'Detail_Wisata' => $row->Detail_Wisata,
			'Tipe_Wisata' => $row->Tipe_Wisata,
			'Gambar_Wisata' => $row->Gambar_Wisata,
			'Jumlah_Votes' => $row->Jumlah_Votes,
			'Tags' => $row->Tags,
			'Lokasi' => $row->Lokasi, 
			'userData'=>$userquery->result(),
			'Flag'=>$flagquery->result()
			);
		}
		
		return $result;

	}
	
	
}
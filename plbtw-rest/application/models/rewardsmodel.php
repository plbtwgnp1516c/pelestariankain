<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Rewardsmodel extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function GetRewardsInfo()
	{
		// $this->db->select()->where('username', $Username);
		// $this->db->from('tbl_user');
		
		// $query = $this->db->get();
		// return $query->result();

		$this->db->select();
		$this->db->from('tbl_rewards');
		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'nama_reward' => $row->nama_reward,
			'deskripsi' => $row->deskripsi,
			// 'asal' => $row->asal,
			'point_needed' => $row->point_needed
			);
		}
		return $result;
	}
	function VoteUp($Email,$ID_Wisata){
		
		$this->db->select('*');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$query = $this->db->get();
		
		$this->db->select('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$queryGetTotalRate = $this->db->get();
		$getTotalRate = $queryGetTotalRate->row("Jumlah_Votes");
		
		$addTotalRate = array(
			'Jumlah_Votes' => $getTotalRate + 1,
		);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'Email' => $Email,
				'ID_Wisata' => $ID_Wisata,
				'Flag' => 2,
			);
			
			$this->db->insert('user_votes',$data);	
		}
		else
		{
			$data=array(	
				'Flag' => 2,
			);
			
			$this->db->update('user_votes',$data,array('Email'	=> $Email,'ID_Wisata' => $ID_Wisata));
		}
		
		$this->db->update('konten_wisata',$addTotalRate,array('ID_Wisata' => $ID_Wisata));	
	}
	
	function VoteDown($Email,$ID_Wisata){
		
		$this->db->select('*');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$query = $this->db->get();
		
		$this->db->select('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$queryGetTotalRate = $this->db->get();
		$getTotalRate = $queryGetTotalRate->row("Jumlah_Votes");
		
		$addTotalRate = array(
			'Jumlah_Votes' => $getTotalRate -1,
		);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'Email' => $Email,
				'ID_Wisata' => $ID_Wisata,
				'Flag' => 1,
			);
			
			$this->db->insert('user_votes',$data);	
		}
		else
		{
			$data=array(	
				'Flag' => 1,
			);
			
			$this->db->update('user_votes',$data,array('Email'	=> $Email,'ID_Wisata' => $ID_Wisata));
		}
		
		$this->db->update('konten_wisata',$addTotalRate,array('ID_Wisata' => $ID_Wisata));	
	}
	
	function VoteNormal($Email,$ID_Wisata){
		
		$this->db->select('*');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$query = $this->db->get();
		
		$this->db->select('Flag');
		$this->db->from('user_votes');
		$this->db->where('Email',$Email);
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$flag_rate_res = $this->db->get();
		$flag_rate_result = $flag_rate_res->row("Flag");
		
		if($flag_rate_result == 1){
			$flagcondition = 1;
		}else if($flag_rate_result == 2){
			$flagcondition = -1;
		}
		
		$this->db->select('Jumlah_Votes');
		$this->db->from('konten_wisata');
		$this->db->where('ID_Wisata',$ID_Wisata);
		
		$queryGetTotalRate = $this->db->get();
		$getTotalRate = $queryGetTotalRate->row("Jumlah_Votes");
		
		$addTotalRate = array(
			'Jumlah_Votes' => $getTotalRate + $flagcondition,
		);
			
		if($query ->num_rows()==0)
		{	
			$data = array(
				'Email' => $Email,
				'ID_Wisata' => $ID_Wisata,
				'Flag' => 0,
			);
			
			$this->db->insert('rate',$data);	
		}
		else
		{
			$data=array(	
				'Flag' => 0,
			);
			
			$this->db->update('user_votes',$data,array('Email'	=> $Email,'ID_Wisata' => $ID_Wisata));
		}
		
		$this->db->update('konten_wisata',$addTotalRate,array('ID_Wisata' => $ID_Wisata));	
	}
}
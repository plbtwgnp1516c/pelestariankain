<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Kontenmodel extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	
	function AddKonten($nama_konten, $jenis, $deskripsi,$desa,$kecamatan,$kabupaten,$provinsi)
	{
		$konten = array(
			'nama_konten' => $nama_konten,
			'jenis' => $jenis,
			'deskripsi' => $deskripsi,
			'desa' => $desa,
			'kecamatan' => $kecamatan,
			'kabupaten' => $kabupaten,
			'provinsi' => $provinsi,
		);
		$this->db->insert('tbl_konten',$konten);
	}


	function GetKonten()
	{
		$this->db->select();
		$this->db->from('tbl_konten');
		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'nama' => $row->nama_konten,
			'jenis' => $row->jenis,
			// 'asal' => $row->asal,
			'deskripsi' => $row->deskripsi
			);
		}
		return $result;
	}
	function GetKontenProvinsi()
	{
		$this->db->select();
		$this->db->from('wilayah_provinsi');
		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'id' => $row->id,
			'nama' => $row->nama

			);
		}
		return $result;
	}
	function GetKontenKabupaten($provinsi_id)
	{
		$this->db->select();
		$this->db->from('wilayah_kabupaten');
		$this->db->where('provinsi_id',$provinsi_id);

		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'provinsi_id' => $row->provinsi_id,
			'id' => $row->id,
			'nama' => $row->nama

			);
		}
		return $result;
	}
	function GetKontenKecamatan($kabupaten_id)
	{
		$this->db->select();
		$this->db->from('wilayah_kecamatan');
		$this->db->where('kabupaten_id',$kabupaten_id);

		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'kabupaten_id' => $row->kabupaten_id,	
			'id' => $row->id,
			'nama' => $row->nama

			);
		}
		return $result;
	}
	function GetKontenDesa($kecamatan_id)
	{

		$this->db->select();
		$this->db->from('wilayah_desa');
		$this->db->where('kecamatan_id',$kecamatan_id);
		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'kecamatan_id' => $row->kecamatan_id,	
			'id' => $row->id,
			'nama' => $row->nama

			);
		}
		return $result;
	}
}
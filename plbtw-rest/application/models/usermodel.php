<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}

	function GetUserInfo($Username)
	{
		$this->db->select()->where('username', $Username);
		$this->db->from('tbl_user');
		
		$query = $this->db->get();
		return $query->result();
	}
	
	function SignUp($Username,$Password,$Nama){
		$data = array(
				'username' => $Username,
				'password' => $Password,
				'nama' => $Nama,

			);
			
		$this->db->insert('tbl_user',$data);
	}
	
	function Login($Username,$Password){
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('username',$Username);
		$this->db->where('password',$Password);
		
		$query = $this->db->get();
		
		if($query ->num_rows()==1)
		{	
			return $query->result();	
		}
		else
		{
			return false;
		}
	}
	
	function GetPoints($Username){
		$this->db->select('points');
		$this->db->from('tbl_user');
		$this->db->where('username',$Username);
		$query = $this->db->get();
		return $query->result();
		
	}
	
	function RedeemChallenge($Username,$addpoint){
		
		
		// $data=array(
		// 			'Challenge1' => true,
		// 			'Jumlah_Medal' => $points_total + $addpoint,
		// 		);
		$this->db->where('username', $Username);
		$this->db->set('points', 'points + '. $addpoint, FALSE);
		$this->db->update('tbl_user');
		// $this->db->update('tbl_user',$data,array('Email'	=> $LoggedEmail));
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('username',$Username);
		$query = $this->db->get();
		return $query->result();
	
		
	}
	
	function RedeemRewards($Username,$minpoints){
		$this->db->where('username', $Username);
		$this->db->set('points', 'points - '. $minpoints, FALSE);
		$this->db->update('tbl_user');
		// $this->db->update('tbl_user',$data,array('Email'	=> $LoggedEmail));
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('username',$Username);
		$query = $this->db->get();
		return $query->result();;
	}
	function GetRewardsInfo()
	{
		// $this->db->select()->where('username', $Username);
		// $this->db->from('tbl_user');
		
		// $query = $this->db->get();
		// return $query->result();

		$this->db->select();
		$this->db->from('tbl_rewards');
		// $this->db->join('tbl_detil_konten', 'tbl_konten.id_detil_konten=tbl_detil_konten.id_detil_konten');
		$query = $this->db->get();
		$result =array();
		foreach($query->result() as $row) {
			$result[] = array(
			'nama_reward' => $row->nama_reward,
			'deskripsi' => $row->deskripsi,
			// 'asal' => $row->asal,
			'point_needed' => $row->point_needed
			);
		}
		return $result;
	}
}
package com.pelestariankain.plbtw.pelestariankain;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentTambahDataFragment extends Fragment {

    public ActivityFragmentTambahDataFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_activity_fragment_tambah_data, container, false);

    }
}

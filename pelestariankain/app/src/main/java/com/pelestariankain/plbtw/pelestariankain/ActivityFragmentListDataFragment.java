package com.pelestariankain.plbtw.pelestariankain;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.pelestariankain.plbtw.pelestariankain.adapter.AdapterKain;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ActivityFragmentListDataFragment extends Fragment {
    private static final String URL_READ="http://10.0.3.2:81/plbtw-rest/index.php/private/pelestariankain/kontenall";
    private static final String URL_CREATEMARTABAK="http://pinjemindong.esy.es/martabakmobile/martabak_controller/create.php";
    private String URLUploadPicture = "http://10.0.3.2:81/webServiceMartabak/upload/savePicture.php";
    private String URLDeletePicture = "http://10.0.3.2:81/webServiceMartabak/upload/deletePhoto.php";

    ProgressDialog pDialog;
    final Context context = getActivity();

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    ArrayList<HashMap<String,String>> arrayListKain;
    public Bitmap gambar;
    Dialog dialog;
    ListView listKain;


    public ActivityFragmentListDataFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_activity_fragment_list_data, container, false);
        arrayListKain = new ArrayList<HashMap<String, String>>();
        listKain = (ListView) v.findViewById(R.id.listView);
        dialog = new Dialog(getActivity());
        new readKainJSON().execute();
        return v;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity)getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    class readKainJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String status,id_pegawai;

        public readKainJSON()
        {
            this.id_pegawai=id_pegawai;
            this.status=status;
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("status",status));
            params.add(new BasicNameValuePair("id_pegawai",id_pegawai));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);

                if(json!=null){
                    sukses=json.getString("status");
                    Log.d("test : ", json.toString());
                    Log.d("test : ", json.getString("kontenData"));
                    Log.d("test : ", json.getJSONArray("kontenData").toString());

                    if(sukses.equalsIgnoreCase("1")){
                        if(json!=null){
                            Log.d("test : ", json.toString());
                            arrayListKain = new ArrayList<HashMap<String, String>>();
                            jsonarray = json.getJSONArray("kontenData");
                            Log.d("test : ", json.toString());

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                String nama = jsonobject.getString("nama");
                                String jenis= jsonobject.getString("jenis");
                                String asal= jsonobject.getString("asal");
                                String deskripsi= jsonobject.getString("deskripsi");

                                Log.d("test : ", nama);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("nama", nama);
                                map.put("jenis", jenis);
                                map.put("asal",asal);
                                map.put("deskripsi",deskripsi);
                                arrayListKain.add(map);
                            }

                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;

        }

        //output
        protected void onPostExecute(String file_url){
            if(sukses.equalsIgnoreCase("1")){
                Log.d("ARRAYY", "ARRAYY --------->" + arrayListKain.toString());
                ListAdapter adapter = new AdapterKain(getActivity(),arrayListKain,
                        R.layout.list_kain,new String[]{"nama","jenis","asal","deskripsi"},
                        new int[] {R.id.fragment_listdata_txtNama,R.id.fragment_listdata_txtJenis});
                listKain.setAdapter(adapter);
            }
        }
    }
}

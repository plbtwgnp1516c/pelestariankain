package com.pelestariankain.plbtw.pelestariankain.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.pelestariankain.plbtw.pelestariankain.JSONParser;
import com.pelestariankain.plbtw.pelestariankain.R;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Joko Adi Hartono on 5/8/2016.
 */
public class AdapterKain extends SimpleAdapter {
    private String URLUploadPicture = "http://10.0.3.2:81/webServiceMartabak/upload/savePicture.php";
    private String URLDeletePicture = "http://10.0.3.2:81/webServiceMartabak/upload/deletePhoto.php";

    public ImageView imageView;
    JSONArray names = null;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    Dialog dialog;
    public Bitmap gambar;

    private ArrayList<HashMap<String, String>> data;
    private Context context;
    String username;
    private LayoutInflater inflater;
    public static ImageView imgView;
    View view;
    private static final int SELECT_PICTURE = 0;

    TextView txtNama;
    TextView txtJenis;
    Button btnDetil;
    public AdapterKain(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to) {
        //super(context, data, resource, from, to);
        super(context, data, resource, from, to);
        Log.d("", "----------->Array " + data);
        this.data = data;
        this.context = context;

    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View vParentRow, ViewGroup parent) {
        view = super.getView(position, vParentRow, parent);
        //View view=vParentRow;
        //if (view == null)
        {
            Log.d("", "--------->POSITION: " + position);
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_kain, parent, false);

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            String url = "http://pinjemindong.esy.es/martabakmobile/images/" + data.get(position).get("id_martabak") + ".jpg";
            Log.d("", "--------->URL" + url);

            Picasso.with(context).load(url).into(imageView);


            txtNama = (TextView) view.findViewById(R.id.fragment_listdata_txtNama);
            txtJenis = (TextView) view.findViewById(R.id.fragment_listdata_txtJenis);
            btnDetil=(Button) view.findViewById(R.id.fragment_listdata_btnDetil);

            txtNama.setText(data.get(position).get("nama"));
            txtJenis.setText(data.get(position).get("jenis"));
            Log.d("cek adapter:", data.get(position).get("jenis"));
            btnDetil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.detaildialog);
                    dialog.setTitle("Detail Kain");
                    TextView dialogTxtNama = (TextView) dialog.findViewById(R.id.detildialog_txtNama);
                    TextView dialogTxtJenis = (TextView) dialog.findViewById(R.id.detildialog_txtJenis);
                    TextView dialogTxtAsal = (TextView) dialog.findViewById(R.id.detildialog_txtAsal);
                    TextView dialogTxtDeskripsi = (TextView) dialog.findViewById(R.id.detildialog_txtDeskripsi);
                    Button dialogBtnClose= (Button)dialog.findViewById(R.id.detildialog_btnClose);
                    dialogTxtNama.setText(data.get(position).get("nama"));
                    dialogTxtJenis.setText(data.get(position).get("jenis"));
                    dialogTxtAsal.setText(data.get(position).get("asal"));
                    dialogTxtDeskripsi.setText(data.get(position).get("deskripsi"));

                    dialogBtnClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
           });

//            final View temp=view;
//            btnUbah.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                       String id_transaksi = (String)lblIdTransaksi.getText();
//                    String id_pegawai = "1";//(String);
//                    dialog = new Dialog(context);
//                    dialog.setContentView(R.layout.detildialog);
//                    dialog.setTitle("Edit Martabak");
//                    EditText edtNama= (EditText) dialog.findViewById(R.id.edtNama);
//                    EditText edtHarga= (EditText) dialog.findViewById(R.id.edtHarga);
//                    Button btnEdtYes=(Button)dialog.findViewById(R.id.btnEdtYes);
//                    Button btnEdtCancel=(Button)dialog.findViewById(R.id.btnEdtCancel);
//                    Button upload=(Button)dialog.findViewById(R.id.btnUploadEdit);
//                    ImageButton imgClose=(ImageButton)dialog.findViewById(R.id.imgBtnClose);
//                    imgView= (ImageView)dialog.findViewById(R.id.imgView);
////                    imgView.setBackgroundResource(null);
//                    Picasso.with(context).load("http://pinjemindong.esy.es/martabakmobile/images/"+idmartabak+".jpg").into(imgView);
//                    edtHarga.setText(lblHarga.getText());
//                    edtNama.setText(lblNama.getText());
//                    btnEdtCancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
//                    btnEdtYes.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            gambar=((BitmapDrawable) imgView.getDrawable()).getBitmap();
//                            new DeleteImage(idmartabak).execute();
//
//                        }
//                    });
//
//                    upload.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent intent = new Intent();
//                            intent.setType("image/*");
//                            intent.setAction(Intent.ACTION_GET_CONTENT);
//                            ((Activity) context).startActivityForResult(Intent.createChooser(intent,
//                                    "Select Picture"), SELECT_PICTURE);
//                        }
//                    });
//                    imgClose.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            imgView.setImageBitmap(null);
//                            imgView.setBackgroundResource(R.drawable.noimagefound);
//                        }
//                    });
//                    dialog.show();
//                    notifyDataSetChanged();
////                    Toast.makeText(v.getContext(), "Selesai", Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
//                });
            return view;
        }
    }
        //#################################KELAS UPLOAD GAMBAR############################################
        class UploadImage extends AsyncTask<Void, Void, String> {
            int sukses = 0;
            String idtrans;

            public UploadImage(String idtrans) {
                this.idtrans = idtrans;
                pDialog = new ProgressDialog(context);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog.setMessage("Please Wait ..., Uploading Images ...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                gambar.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//                nameValuePairs.add(new BasicNameValuePair("base64", encodedImage));
//                nameValuePairs.add(new BasicNameValuePair("ImageName", idtrans + ".jpg"));
//                nameValuePairs.add(new BasicNameValuePair("id", idtrans));
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(URLUploadPicture);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    String st = EntityUtils.toString(response.getEntity());
                } catch (Exception e) {
                    Log.v("log_tag", "Error in http connection " + e.toString());
                }
                return "Success";
            }


            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);
                pDialog.dismiss();
                dialog.dismiss();
            }
        }
        class DeleteImage extends AsyncTask<Void, Void, String> {
            int sukses = 0;
            String idtrans;

            public DeleteImage(String idtrans) {
                this.idtrans = idtrans;
                pDialog = new ProgressDialog(context);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog.setMessage("Please Wait ..., Uploading Images ...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
            }

            @Override
            protected String doInBackground(Void... voids) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                gambar.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("name", idtrans + ".jpg"));
                nameValuePairs.add(new BasicNameValuePair("id", idtrans));

                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(URLDeletePicture);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    String st = EntityUtils.toString(response.getEntity());
                } catch (Exception e) {
                    Log.v("log_tag", "Error in http connection " + e.toString());
                }
                return "Success";
            }


            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);
                pDialog.dismiss();
                new UploadImage(idtrans).execute();
            }

        }

}



